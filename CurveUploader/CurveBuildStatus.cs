﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CurveUploader
{
    public class CurveBuildStatus
    {
        public String ZEMACurveName
        { get; set; }


        public String CCICurveName
        { get; set; }

        public String BuildStatus
        { get; set; }

        public String ValidationStatus
        { get; set; }
        public String Message
        { get; set; }


    }
}
