﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using ExcelDna.Integration;
using System.Windows.Forms;
using CurveUploader.GUI;


namespace CurveUploader
{
    public class CurveBuildUtility
    {
        public List<CurveBuildStatus> BuildAllCurves(bool displayMessageBox=true)
        {
            IDictionary<string, CurveMarksBO> curveDictionary = CurveMarksHelper.CurveUploadDictionary;
            List<String> shortNameList = new List<String>();
            List<CurveBuildStatus> curveBuildStatusList = new List<CurveBuildStatus>();

            CurveMarksHelper.CurveBuildStatusList = new List<CurveBuildStatus>();

            foreach (KeyValuePair<string, CurveMarksBO> kvp in curveDictionary)
            {
                if (kvp.Value.ContextID.Equals(Global.CCI_CONTEXT))
                {
                    shortNameList.Add(kvp.Value.CurveShortName);
                }
            }

            return BuildCurves(shortNameList);            

        }

        public List<CurveBuildStatus> BuildSingleCurve(String curveShortName)
        {
            return BuildCurves(new List<String>(new String[] { curveShortName }));
        }

        private List<CurveBuildStatus> BuildCurves(List<String> shortNameList, bool displayMessageBox = true)
        {
            IDictionary<string, CurveMarksBO> curveDictionary = CurveMarksHelper.CurveUploadDictionary;
            List<CurveBuildStatus> curveBuildStatusList = new List<CurveBuildStatus>();
            CurveMarksHelper.CurveBuildStatusList.Clear();


            CurveGateway gateway = new CurveGateway(Global.CurveWebServiceUrl);

            ((Microsoft.Office.Interop.Excel.Application)ExcelDnaUtil.Application).Cursor = Microsoft.Office.Interop.Excel.XlMousePointer.xlWait;
            ((Microsoft.Office.Interop.Excel.Application)ExcelDnaUtil.Application).StatusBar = "Building Curves...";
            ((Microsoft.Office.Interop.Excel.Application)(ExcelDnaUtil.Application)).ScreenUpdating = false;
            try
            {
                if (shortNameList.Count > 0)
                {
                    curveBuildStatusList = gateway.BuildCurves(shortNameList, DateTime.Now);
                    CurveMarksHelper.CurveBuildStatusList = curveBuildStatusList;

                    if (CurveMarksHelper.CurveBuildStatusList.Any<CurveBuildStatus>(x => x.BuildStatus.ToUpper() == "ERROR" || x.ValidationStatus.ToUpper() == "ERROR" || x.BuildStatus.ToUpper() == "MISSING"))
                    {
                        if (Global.DisplayMsgYesNo("Some curves did not build successfully. Do you want to view results?", displayMessageBox))
                        {
                            FormCurveBuildStatus cbs = new FormCurveBuildStatus();
                            cbs.ShowDialog();
                        }

                    }
                    //else
                    //    Global.DisplayMsg("All Curves were built successfully.");
                }

            }
            finally
            {
                ((Microsoft.Office.Interop.Excel.Application)ExcelDnaUtil.Application).StatusBar = "Ready";
                ((Microsoft.Office.Interop.Excel.Application)ExcelDnaUtil.Application).Cursor = Microsoft.Office.Interop.Excel.XlMousePointer.xlDefault;
                ((Microsoft.Office.Interop.Excel.Application)(ExcelDnaUtil.Application)).ScreenUpdating = true;
            }
            return curveBuildStatusList;



        }
    }
}
