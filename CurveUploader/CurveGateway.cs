﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text.RegularExpressions;
using log4net;
using System.Windows.Forms;
using CurveUploader.CurveService;
using System.Diagnostics;

namespace CurveUploader
{
    public class CurveGateway
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(CurveGateway));

        private string webServiceUrl = string.Empty;

        private BlockingCollection<CurveSubmitStatus> curveStatusList = null;
        private BlockingCollection<DownloadCurveParams> downloadCurveList = null;

        private ConcurrentQueue<CurveMark> curveQueue = null;
        private ConcurrentQueue<DownloadCurveParams> curveDownloadQueue = null;

        private bool curveSubmitErrorShown = false;

        public CurveGateway(string url)
        {
            webServiceUrl = url;
            curveQueue = new ConcurrentQueue<CurveMark>();
            curveDownloadQueue = new ConcurrentQueue<DownloadCurveParams>();

            curveStatusList = new BlockingCollection<CurveSubmitStatus>();
            downloadCurveList = new BlockingCollection<DownloadCurveParams>();

            Global.ConnectionInitialized = true;
        }

        private void Send(bool displayMessageBox = true)
        {
            CurveMark cm = null;

            CurveService.CurveServiceClient curveServiceClient = new CurveService.CurveServiceClient(GetBinding(), GetEndpointAddress());

            while (curveQueue.TryDequeue(out cm))
            {
                if (cm != null)
                {
                    try
                    {

                        if (cm.ForwardCurveType.ToUpper() == "PRICE")
                        {
                            string status = curveServiceClient.uploadCurve(cm.CurveMasterShortName,
                                                            cm.CurveSet,
                                                            cm.CurveStyle,
                                                            cm.ForwardCurveType,
                                                            cm.BlockName,
                                                            cm.MarkDate,
                                                            cm.CreateUserId,
                                                            cm.Periodicity,
                                                            cm.ClientId,
                                                            cm.RequestId,
                                                            cm.DateList.ToArray<string>(),
                                                            cm.PriceList.ToArray<string>(),
                                                            cm.ContextID);
                            CurveSubmitStatus curveStatus = new CurveSubmitStatus(cm.RequestId, status.Contains("ERROR") ? false : true,
                                                                    status.Contains("ERROR") ? status : "SUCCESS");
                            curveStatusList.Add(curveStatus);
                        }
                        else if (cm.ForwardCurveType.ToUpper() == "VOLATILITY")
                        {
                            string status = curveServiceClient.uploadVolatilityCurve(cm.CurveMasterShortName,
                                                                                     cm.CurveSet,
                                                                                     cm.CurveStyle,
                                                                                     cm.ForwardCurveType,
                                                                                     cm.BlockName,
                                                                                     cm.StrikePrice,
                                                                                     cm.MarkDate,
                                                                                     cm.CreateUserId,
                                                                                     cm.Periodicity,
                                                                                     cm.ClientId,
                                                                                     cm.RequestId,
                                                                                     cm.DateList.ToArray<string>(),
                                                                                     null,
                                                                                     cm.PriceList.ToArray<string>(),
                                                                                     null,
                                                                                     null,
                                                                                     null,
                                                                                     null);
                            CurveSubmitStatus curveStatus = new CurveSubmitStatus(cm.RequestId, status.Contains("ERROR") ? false : true, status.Contains("ERROR") ? status : "SUCCESS");
                            curveStatusList.Add(curveStatus);
                        }
                        else if (cm.ForwardCurveType.ToUpper() == Global.VOLS)
                        {
                            string status = curveServiceClient.uploadVolatilities(cm.CurveMasterShortName,
                                                                                     cm.CurveSet,
                                                                                     cm.ContextID,
                                                                                     cm.MarkDate,
                                                                                     cm.CreateUserId,
                                                                                     cm.RequestId,
                                                                                     cm.DateList.ToArray<string>(),
                                                                                     null,                                                                                     
                                                                                     cm.Coeff1List.ToArray<string>(),
                                                                                     cm.Coeff2List.ToArray<string>(),
                                                                                     cm.Coeff3List.ToArray<string>(),
                                                                                     cm.Coeff4List.ToArray<string>(),
                                                                                     cm.Coeff5List.ToArray<string>(),
                                                                                     cm.Coeff6List.ToArray<string>(),
                                                                                     cm.Coeff7List.ToArray<string>());
                            CurveSubmitStatus curveStatus = new CurveSubmitStatus(cm.RequestId, status.Contains("ERROR") ? false : true, status.Contains("ERROR") ? status : "SUCCESS");
                            curveStatusList.Add(curveStatus);
                        }
                    }
                    catch (Exception ex)
                    {
                        _log.Error("CurveGateway::Send ", ex);
                        if (displayMessageBox && !curveSubmitErrorShown)
                        {
                            curveSubmitErrorShown = true;
                            MessageBox.Show("Severe error. Server could be down." + Environment.NewLine + "Error message is: " + ex.Message, Global.MessageBoxTitle);

                        }
                    }
                }
            }
        }

        public List<CurveSubmitStatus> Send(List<CurveMark> curveMarksList, bool displayMessageBox = true)
        {

            curveMarksList.ForEach(cm => curveQueue.Enqueue(cm));

            Task[] tasks = new Task[curveMarksList.Count > 10 ? 10 : curveMarksList.Count];

            curveSubmitErrorShown = false;

            for (int i = 0; i < tasks.Length; i++)
            {
                tasks[i] = Task.Factory.StartNew(() => Send(displayMessageBox));

            }

            //Wait for a maximum of 10 seconds * Number of Curves
            bool allTasksSucceeded = Task.WaitAll(tasks, 1000 * 10 * curveMarksList.Count());

            if (!allTasksSucceeded)
            {
                _log.Error("Task.WaitAll expired befored all curves were successfully submitted.");
                //MessageBox.Show("Task.WaitAll expired befored all curves were successfully submitted.", Global.MessageBoxTitle);
            }

            curveSubmitErrorShown = false;
            return curveStatusList.ToList<CurveSubmitStatus>();



        }

        private void DownloadCurve()
        {
            DownloadCurveParams downloadRequest = null;

            CurveService.CurveServiceClient curveServiceClient = new CurveService.CurveServiceClient(GetBinding(), GetEndpointAddress());

            while (curveDownloadQueue.TryDequeue(out downloadRequest))
            {
                if (downloadRequest != null)
                {
                    try
                    {

                        String downloadCurve = curveServiceClient.downloadCurve(downloadRequest.ShortName,
                                                         downloadRequest.CurveSet,
                                                         downloadRequest.CurveStyle,
                                                         downloadRequest.ForwardCurveType,
                                                         downloadRequest.BlockName,
                                                         downloadRequest.StrikePrice,
                                                         downloadRequest.MarkDate,
                                                         downloadRequest.StartDate,
                                                         downloadRequest.EndDate,
                                                         downloadRequest.ContextID);

                        downloadRequest.CurveData = downloadCurve;

                        downloadCurveList.Add(downloadRequest);
                    }
                    catch (Exception ex)
                    {
                        _log.Error("CurveGateway::Download ", ex);
                    }
                }
            }
        }

        public List<DownloadCurveParams> DownloadCurves(List<DownloadCurveParams> reqParamsList)
        {
            reqParamsList.ForEach(rp => curveDownloadQueue.Enqueue(rp));

            Task[] tasks = new Task[reqParamsList.Count > 10 ? 10 : reqParamsList.Count];



            for (int i = 0; i < tasks.Length; i++)
            {
                tasks[i] = Task.Factory.StartNew(() => DownloadCurve());

            }



            //Wait for a maximum of 10 seconds * Number of Curves
            bool allTasksSucceeded = Task.WaitAll(tasks, 1000 * 10 * reqParamsList.Count());

            if (!allTasksSucceeded)
            {
                _log.Error("Task.WaitAll expired befored all curves were successfully downloaded.");
                //MessageBox.Show("Task.WaitAll expired befored all curves were successfully submitted.", Global.MessageBoxTitle);
            }

            return downloadCurveList.ToList<DownloadCurveParams>();
        }

        public List<CurveBuildStatus> BuildCurves(List<String> cciCurveNamesList, DateTime effectiveDate)
        {
            List<CurveBuildStatus> curveBuildStatusList = new List<CurveBuildStatus>();

            CurveService.CurveServiceClient curveServiceClient = new CurveService.CurveServiceClient(GetBinding(), GetEndpointAddress());
            zemaCurveBuildStatus[] curveBuildStatus = curveServiceClient.buildCurve(cciCurveNamesList.ToArray<String>(), effectiveDate.ToString("yyyyMMdd"));

            foreach (var item in curveBuildStatus)
            {
                CurveBuildStatus cbs = new CurveBuildStatus();
                if (!string.IsNullOrEmpty(item.zemaCurve))
                {
                    cbs.ZEMACurveName = item.zemaCurve;
                    cbs.CCICurveName = item.cciCurve;
                    cbs.BuildStatus = item.buildStatus;
                    cbs.ValidationStatus = item.validationStatus;
                    cbs.Message = item.message;
                }
                else if (!string.IsNullOrEmpty(item.message))
                    cbs.Message = item.message;

                curveBuildStatusList.Add(cbs);
            }



            return curveBuildStatusList;
        }

        public EndpointAddress GetEndpointAddress()
        {
            return new EndpointAddress(webServiceUrl);
        }

        public System.ServiceModel.Channels.Binding GetBinding()
        {
            BasicHttpBinding binding = new BasicHttpBinding();
            binding.MaxReceivedMessageSize = 2147483647;
            binding.ReaderQuotas.MaxStringContentLength = 2147483647;
            binding.ReaderQuotas.MaxArrayLength = 2147483647;
            binding.MaxBufferPoolSize = 2147483647;
            binding.MaxBufferSize = 2147483647;
            binding.TransferMode = TransferMode.Streamed;
            binding.SendTimeout = binding.ReceiveTimeout = TimeSpan.FromMinutes(20);

            return binding;
        }

        /// <summary>
        /// Gets the service details related to the service.
        /// </summary>
        /// <returns>the service details string</returns>
        public string HeartBeatFunctionality()
        {
            string serviceDetails = string.Empty;
            CurveService.CurveServiceClient curveServiceClient = new CurveService.CurveServiceClient(GetBinding(), GetEndpointAddress());
            try
            {
                serviceDetails = curveServiceClient.getServiceVersion() + " is up at link: " + Global.CurveWebServiceUrl;
            }
            catch (Exception ex)
            {
                serviceDetails = "Curve WebService is down at link: " + Global.CurveWebServiceUrl;
                _log.Error(ex);
            }
            return serviceDetails;
        }

        /// <summary>
        /// Gets the list of curve region and context
        /// </summary>
        public string GetCurveData(string key)
        {
            string regionContextList = string.Empty;
            CurveService.CurveServiceClient curveServiceClient = new CurveService.CurveServiceClient(GetBinding(), GetEndpointAddress());
            try
            {
                regionContextList = curveServiceClient.getCurveData(key);
            }
            catch (Exception ex)
            {
                regionContextList = "Curve WebService is down at link: " + Global.CurveWebServiceUrl;
                _log.Error(ex);
            }
            return regionContextList;
        }

        /// <summary>
        /// Gets the sign off release history list
        /// </summary>
        public string GetSignOffData(string region, string context)
        {
            string signoffData = string.Empty;
            CurveService.CurveServiceClient curveServiceClient = new CurveService.CurveServiceClient(GetBinding(), GetEndpointAddress());
            try
            {
                signoffData = curveServiceClient.getSignOffData(context,region);
            }
            catch (Exception ex)
            {
                signoffData = "Curve WebService is down at link: " + Global.CurveWebServiceUrl;
                _log.Error(ex);
            }
            return signoffData;
        }

        /// <summary>
        /// Sets the sign off release history 
        /// </summary>
        public string SendSignOffData(CurveSignOffParams curveSignOffParams)
        {
            string signoffData = string.Empty;
            CurveService.CurveServiceClient curveServiceClient = new CurveService.CurveServiceClient(GetBinding(), GetEndpointAddress());
            try
            {
                signoffData = curveServiceClient.sendSignOff(curveSignOffParams.Region,
                                                             curveSignOffParams.ContextID,
                                                             curveSignOffParams.MarkDate,
                                                             curveSignOffParams.UserID,
                                                             curveSignOffParams.SignOffType,
                                                             curveSignOffParams.Reason);
            }
            catch (Exception ex)
            {
                signoffData = "Curve WebService is down at link: " + Global.CurveWebServiceUrl;
                _log.Error(ex);
            }
            return signoffData;
        }

        /// <summary>
        /// Gets folder path for zema profile files
        /// </summary>
        public string GetProfileFolderPath(string propertyName)
        {
            string profileFolderPath = string.Empty;
            CurveService.CurveServiceClient curveServiceClient = new CurveService.CurveServiceClient(GetBinding(), GetEndpointAddress());
            try
            {
                profileFolderPath = curveServiceClient.getConfigData(propertyName);
            }
            catch (Exception ex)
            {
                profileFolderPath = "Curve WebService is down at link: " + Global.CurveWebServiceUrl;
                _log.Error(ex);
            }
            return profileFolderPath;
        }

        public string Refresh(string context)
        {
            string result = string.Empty;
            CurveService.CurveServiceClient curveServiceClient = new CurveService.CurveServiceClient(GetBinding(), GetEndpointAddress());
            try
            {
                result = curveServiceClient.refreshFromTibco(context);
            }
            catch (Exception ex)
            {
                result = "Curve WebService is down at link: " + Global.CurveWebServiceUrl;
                _log.Error(ex);
            }
            return result;
        }
        

        #region Commented historical methods

        //public List<DownloadCurveParams> DownloadHistoricalCurves(List<DownloadCurveParams> reqParamsList)
        //{
        //    reqParamsList.ForEach(rp => curveDownloadQueue.Enqueue(rp));

        //    Task[] tasks = new Task[reqParamsList.Count > 10 ? 10 : reqParamsList.Count];

        //    for (int i = 0; i < tasks.Length; i++)
        //    {
        //        tasks[i] = Task.Factory.StartNew(() => DownloadHistoricalCurve());

        //    }

        //    bool allTasksSucceeded = Task.WaitAll(tasks, 300 * 1000);
        //    //Wait for a maximum of 10 seconds * Number of Curves
        //    // bool allTasksSucceeded = Task.WaitAll(tasks, 1000 * 10 * reqParamsList.Count()); // TODO : Uncomment the code

        //    //Task.WaitAll(tasks);// TODO : Comment the code, just for trial

        //    if (!allTasksSucceeded) 
        //    {
        //        _log.Error("Task.WaitAll expired befored all curves were successfully downloaded."); // TODO : Uncomment the code
        //        //MessageBox.Show("Task.WaitAll expired befored all curves were successfully submitted.", Global.MessageBoxTitle);
        //    }

        //    return downloadCurveList.ToList<DownloadCurveParams>();
        //}

        //private void DownloadHistoricalCurve()
        //{
        //    DownloadCurveParams downloadRequest = null;

        //    CurveService.CurveServiceClient curveServiceClient = new CurveService.CurveServiceClient(GetBinding(), GetEndpointAddress());

        //    while (curveDownloadQueue.TryDequeue(out downloadRequest))
        //    {
        //        if (downloadRequest != null)
        //        {
        //            try
        //            {
        //                DateTime start = DateTime.Now;
        //                _log.Info(downloadRequest.FunctionName + " calls the service");
        //                String downloadCurve = curveServiceClient.downloadCurveHistory(downloadRequest.ShortName,
        //                                                 downloadRequest.CurveSet,                                                         
        //                                                 downloadRequest.StartDate,
        //                                                 downloadRequest.EndDate,
        //                                                 downloadRequest.ContextID,
        //                                                 Environment.UserName,
        //                                                 downloadRequest.FunctionName);

        //                downloadRequest.CurveData = downloadCurve;

        //                downloadCurveList.Add(downloadRequest);
        //                DateTime end = DateTime.Now;
        //                TimeSpan timeTaken = end - start;
        //                _log.InfoFormat("Time taken to fetch the data from the service:{0}secs for {1}",timeTaken.Seconds, downloadRequest.FunctionName);
        //            }
        //            catch (Exception ex)
        //            {
        //                Global.ServiceHostException = ex.InnerException.Message;
        //                _log.Error("CurveGateway::Download ", ex);
        //            }
        //        }
        //    }
        //}

        #endregion
    }
}
