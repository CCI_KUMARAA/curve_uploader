﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CurveUploader
{
    public class CurveMark
    {
        public string CurveMasterShortName
        {
            get;
            set;
        }

        public string CurveSet
        {
            get;
            set;
        }

        public String CurveStyle
        {
            get;
            set;
        }

        public string ForwardCurveType
        {
            get;
            set;
        }

        public string BlockName
        {
            get;
            set;

        }

        public string StrikePrice
        {
            get;
            set;
        }

        public string MarkDate
        {
            get;
            set;
        }

        public string CreateUserId
        {
            get;
            set;
        }

        public string Periodicity
        {
            get;
            set;
        }

        public string ClientId
        {
            get;
            set;

        }

        public string RequestId
        {
            get;
            set;
        }

        public List<string> DateList
        {
            get;
            set;
        }

        public List<string> Date2List
        {
            get;
            set;
        }

        public List<string> PriceList
        {
            get;
            set;
        }

        public List<string> Coeff1List
        {
            get;
            set;

        }

        public List<string> Coeff2List
        {
            get;
            set;
        }

        public List<string> Coeff3List
        {
            get;
            set;

        }

        public List<string> Coeff4List
        {
            get;
            set;
        }

        public List<string> Coeff5List
        {
            get;
            set;
        }

        public List<string> Coeff6List
        {
            get;
            set;
        }

        public List<string> Coeff7List
        {
            get;
            set;
        }       

        public string ContextID { get; set; }
    }

    public class CurveMarkPoint
    {
        public string Date
        {
            get;
            set;
        }

        public string Date2
        {
            get;
            set;
        }

        public string Price
        {
            get;
            set;

        }

        public string Coeff1
        {
            get;
            set;
        }

        public string Coeff2
        {
            get;
            set;
        }

        public string Coeff3
        {
            get;
            set;
        }

        public string Coeff4
        {
            get;
            set;

        }

        public string Coeff5
        {
            get;
            set;

        }

        public string Coeff6
        {
            get;
            set;

        }

        public string Coeff7
        {
            get;
            set;

        }

    }
}
