﻿using System;
using Excel = Microsoft.Office.Interop.Excel;
using System.Collections.Generic;


namespace CurveUploader
{
    /// <summary>
    /// CurveMarksBO differs in the serializable CurveMarksType class in that this class 
    /// contains additional curve information which is not serialized.  (i.e. curve status)
    /// </summary>
    public class CurveMarksBO
    {
        private bool isPipelineCurveMark;

        public bool IsPipelineCurveMark
        {
            get
            {
                return isPipelineCurveMark;
            }
            set { isPipelineCurveMark = value; }
        }

        private Excel.Range callingCell;

        public Excel.Range CallingCell
        {
            get { return callingCell; }
            set { callingCell = value; }
        }

        private Excel.Range downloadCell;

        public Excel.Range DownloadCell
        {
            get { return downloadCell; }
            set { downloadCell = value; }
        }

        private string periodicity;

        public string Periodicity
        {
            get { return periodicity; }
            set { periodicity = value; }
        }

        private Excel.Range dateRange;

        public Excel.Range DateRange
        {
            get { return dateRange; }
            set { dateRange = value; }
        }

        private Excel.Range atmVolRange;

        public Excel.Range AtmVolRange
        {
            get { return atmVolRange; }
            set { atmVolRange = value; }
        }

        private Excel.Range dateRange2;

        public Excel.Range DateRange2
        {
            get { return dateRange2; }
            set { dateRange2 = value; }
        }

        private Excel.Range valueRange;

        public Excel.Range ValueRange
        {
            get { return valueRange; }
            set { valueRange = value; }
        }

        private string curveShortName;

        public string CurveShortName
        {
            get { return curveShortName; }
            set { curveShortName = value; }
        }

        private string valueStrike;

        public string ValueStrike
        {
            get { return valueStrike; }
            set { valueStrike = value; }
        }

        private string forwardCurveType;

        public string ForwardCurveType
        {
            get { return forwardCurveType; }
            set { forwardCurveType = value; }
        }

        private string curveSet;

        public string CurveSet
        {
            get { return curveSet; }
            set { curveSet = value; }
        }

        private string markDate;

        public string MarkDate
        {
            get { return markDate; }
            set { markDate = value; }
        }

        private string curveStyle;

        public string CurveStyle
        {
            get { return curveStyle; }
            set { curveStyle = value; }
        }

        private string blockName;

        public string BlockName
        {
            get { return blockName; }
            set { blockName = value; }
        }

        private string curveStatus;

        public string CurveStatus
        {
            get { return curveStatus; }
            set { curveStatus = value; }
        }

        private string curveStatusMsg;

        public string CurveStatusMsg
        {
            get { return curveStatusMsg; }
            set { curveStatusMsg = value; }
        }

        private DateTime curveStatusDateTime;

        public DateTime CurveStatusDateTime
        {
            get { return curveStatusDateTime; }
            set { curveStatusDateTime = value; }
        }

        private string startDate;

        public string StartDate
        {
            get { return startDate; }
            set { startDate = value; }
        }

        private string endDate;

        public string EndDate
        {
            get { return endDate; }
            set { endDate = value; }
        }

        public List<string> PipelineCycleCurveDates
        {
            get;
            set;
        }

        public List<string> PipelineCycleCurveValues
        {
            get;
            set;
        }

        private string contextID;
        /// <summary>
        /// Get's and set's the context id value.
        /// </summary>
        public string ContextID
        {
            get { return contextID; }
            set { contextID = value; }
        }        

    }
}
