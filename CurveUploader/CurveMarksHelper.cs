﻿using System;
using System.Linq;
using System.Collections.Concurrent;
using Excel = Microsoft.Office.Interop.Excel;
using System.DirectoryServices.AccountManagement;
using System.Collections.Generic;

namespace CurveUploader
{
    public  static class CurveMarksHelper
    {
        // Cached dictionary of registered curves for Upload
        private static ConcurrentDictionary<string, CurveMarksBO> curveUploadDictionary =
            new ConcurrentDictionary<string, CurveMarksBO>();

        private static List<CurveBuildStatus> curveBuildStatusList = new List<CurveBuildStatus>();        

        public static List<CurveBuildStatus> CurveBuildStatusList
        {
            get
            {
                return curveBuildStatusList;
            }
            set
            {
                curveBuildStatusList = value;
            }
        }

        public static ConcurrentDictionary<string, CurveMarksBO> CurveUploadDictionary
        {
            get { return CurveMarksHelper.curveUploadDictionary; }
            set { CurveMarksHelper.curveUploadDictionary = value; }
        }

        private static ConcurrentDictionary<string, CurveMarksBO> curveDownloadDictionary =
            new ConcurrentDictionary<string, CurveMarksBO>();

        public static ConcurrentDictionary<string, CurveMarksBO> CurveDownloadDictionary
        {
            get { return CurveMarksHelper.curveDownloadDictionary; }
            set { CurveMarksHelper.curveDownloadDictionary = value; }
        }

        private static ConcurrentDictionary<string, List<string>> curveRegionContextDictionary =
           new ConcurrentDictionary<string, List<string>>();

        public static ConcurrentDictionary<string, List<string>> CurveRegionContextDictionary
        {
            get { return CurveMarksHelper.curveRegionContextDictionary; }
            set { CurveMarksHelper.curveRegionContextDictionary = value; }
        }

        internal static bool IsCurveUploadUser(string userId)
        {
            return IsADGroupMember(userId, Global.CurveUploadGroup);
        }

        internal static bool IsCurveUploadAdmin(string userId)
        {
            return IsADGroupMember(userId, Global.CurveUploadAdmin);
        }

        private static bool IsADGroupMember(string userId, string adGroup)
        {
            PrincipalContext ctx = new PrincipalContext(ContextType.Domain, Environment.UserDomainName);
            GroupPrincipal grp = GroupPrincipal.FindByIdentity(ctx, IdentityType.Name, adGroup);

            return grp.GetMembers(true).Where<Principal>(p => p.SamAccountName.ToLower() == userId.ToLower()).Count() > 0;

        }

        internal static void ReleaseComObject(object o)
        {
            if (o == null)
                return;
            
         
            try
            {
                System.Runtime.InteropServices.Marshal.FinalReleaseComObject(o);
            }
            catch { }
            finally
            {
                o = null;
            }
        }

        public static void UpdateMarkDate()
        {
            foreach(string key in CurveUploadDictionary.Keys)
            {
                CurveMarksBO cm = CurveUploadDictionary[key];

                cm.MarkDate = Global.MarkDate;
            }
        }
    }
}
