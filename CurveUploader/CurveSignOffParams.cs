﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CurveUploader
{
    public class CurveSignOffParams
    {
        public string Region
        {
            get;
            set;
        }
        public string ContextID
        {
            get;
            set;
        }
        public string MarkDate
        {
            get;
            set;
        }
        public string UserID
        {
            get;
            set;
        }
        public string SignOffType
        {
            get;
            set;
        }
        public string Reason
        {
            get;
            set;
        }
    }
}
