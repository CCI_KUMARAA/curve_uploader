﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CurveUploader
{
    public class CurveSubmitStatus
    {
        public string RequestId
        {
            get;
            private set;
        }

        public bool Success
        {
            get;
            private set;
        }

        public string Message
        {
            get;
            private set;
        }

        public CurveSubmitStatus(string requestId, bool success, string message)
        {
            RequestId = requestId;
            Success = success;
            Message = message;
        }

    }
}
