﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.Concurrent;
using System.Threading;
using System.IO;
using System.Xml.Serialization;
using ExcelDna.Integration;
using log4net;
using System.Windows.Forms;
using System.Globalization;
using Excel = Microsoft.Office.Interop.Excel;
using Microsoft.Office.Interop.Excel;
using System.Collections;
using System.Data;
using System.Diagnostics;

namespace CurveUploader
{
    public class CurveUploadUtility
    {
        public bool isUploadMultipleMonthlyCurves = false;
        public bool isDownloadMultipleMonthlyCurves = false;     

        private static readonly ILog _log = LogManager.GetLogger(typeof(CurveUploadUtility));

        public Tuple<int, int> SubmitCurveList(List<CurveMark> curveMarksList, bool displayMessageBox = true)
        {
            CurveGateway gateway = new CurveGateway(Global.CurveWebServiceUrl);

            List<CurveSubmitStatus> curveStatusList = gateway.Send(curveMarksList, displayMessageBox);

            curveStatusList.ForEach(status => UpdateCurveStatusAsync(status));

            int successCount = curveStatusList.Where<CurveSubmitStatus>(css => css.Success).Count();
            int failedCount = curveStatusList.Where<CurveSubmitStatus>(css => !css.Success).Count();

            return Tuple.Create(successCount, failedCount);
        }

        void UpdateCurveStatusAsync(CurveSubmitStatus status)
        {

            Excel.Worksheet worksheet = null;
            Excel.Interior i = null;
            Excel.Range r = null;
            try
            {
                string key = status.RequestId;
                string[] tokens = key.Split('@');
                string workbook = tokens[0];
                string worksheetIndex = tokens[1];
                string cell = tokens[2];

                string msg = status.Message;

                if (CurveMarksHelper.CurveUploadDictionary.ContainsKey(key))
                {
                    CurveMarksBO curveMarksBO = CurveMarksHelper.CurveUploadDictionary[key];
                    curveMarksBO.CurveStatus = status.Success ? "SUCCESS" : "ERROR";
                    curveMarksBO.CurveStatusMsg = msg;
                    curveMarksBO.CurveStatusDateTime = DateTime.Now;
                }

                Excel.Application app = (Excel.Application)ExcelDnaUtil.Application;

                worksheet = app.Worksheets[Convert.ToInt32(worksheetIndex)];

                r = (Excel.Range)worksheet.get_Range(cell, cell);
                i = r.Interior;

                i.ColorIndex = status.Success ? 43 : 3;
            }
            catch (Exception ex)
            {
                _log.Error("CurveUploadUtility::UpdateCurveStatus ", ex);
            }
            finally
            {
                CurveMarksHelper.ReleaseComObject(i);
                CurveMarksHelper.ReleaseComObject(r);
                CurveMarksHelper.ReleaseComObject(worksheet);


            }


        }

        public void ResetSingleCurveStatus(string singleCurveKey)
        {
            Excel.Worksheet worksheet = null;
            Excel.Range r2 = null;
            Excel.Interior i = null;

            try
            {
                // Reset color functions in all cells registered for Curve Upload
                if (CurveMarksHelper.CurveUploadDictionary.ContainsKey(singleCurveKey))
                {
                    CurveMarksBO curveMarksBO = CurveMarksHelper.CurveUploadDictionary[singleCurveKey];

                    string[] tokens = singleCurveKey.Split('@');
                    string workbook = tokens[0];
                    string worksheetIndex = tokens[1];
                    string cell = tokens[2];

                    Excel.Application app = (Excel.Application)ExcelDnaUtil.Application;
                    worksheet = app.Worksheets[Convert.ToInt32(worksheetIndex)];

                    r2 = (Excel.Range)worksheet.get_Range(cell, cell);

                    i = r2.Interior;
                    i.ColorIndex = 2;

                    curveMarksBO.CurveStatus = Global.CurveUploadStatusPending;
                    curveMarksBO.CurveStatusMsg = "Curve Not Uploaded";
                    curveMarksBO.CurveStatusDateTime = DateTime.Now;
                }
            }
            catch (Exception ex)
            {
                _log.Error("CurveUploadUtility::ResetSingleCurveStatus() ", ex);
            }
            finally
            {
                CurveMarksHelper.ReleaseComObject(i);
                CurveMarksHelper.ReleaseComObject(r2);
                CurveMarksHelper.ReleaseComObject(worksheet);
            }
        }

        internal bool UploadCurveList(string singleCurve, bool intraday, bool displayStatusMessageBox = true)
        {
            if (Global.MarkDate != null)
            {
                if (displayStatusMessageBox)
                {
                    DialogResult result = MessageBox.Show("Warning - Mark Date is not the Current Date. Continue?",
                        Global.MessageBoxTitle, MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                    if (result == DialogResult.No)
                        return false;
                }
                else
                {
                    _log.Error("Warning - Mark Date is not the Current Date. Exiting Curve Upload Process without submitting Curves.");
                    return false;
                }
            }
            
            ((Excel.Application)ExcelDnaUtil.Application).Cursor = Excel.XlMousePointer.xlWait;
            ((Excel.Application)ExcelDnaUtil.Application).StatusBar = "Uploading Curves...";

            RemoveEmptyCellCurves(CurveMarksHelper.CurveUploadDictionary);

            int uploadCount = 0;

            string key = null;

            // List of serializable curve objects
            List<CurveMark> curveMarkList = new List<CurveMark>();

            IDictionary<string, CurveMarksBO> tempCurveDictionary;

            Excel.Worksheet worksheet = null;
            Excel.Range r2 = null;
            Object obj = null;

            try
            {
                //If for some reason it is reported that there are no curves in the dictionary, recalc
                if (CurveMarksHelper.CurveUploadDictionary.Count <= 0)
                {
                    ((Excel.Application)ExcelDnaUtil.Application).Application.Calculate();
                }

                // Determine if uploading single curve or entire list
                if (singleCurve != null)
                {
                    tempCurveDictionary = new Dictionary<string, CurveMarksBO>();
                    string[] tokens = singleCurve.Split('$');
                    string singleCurveKey = tokens[tokens.Length - 1];
                    if (CurveMarksHelper.CurveUploadDictionary.ContainsKey(singleCurveKey))
                    {
                        tempCurveDictionary.Add(singleCurveKey, CurveMarksHelper.CurveUploadDictionary[singleCurveKey]);
                    }
                    ResetSingleCurveStatus(singleCurveKey);
                }
                else
                {
                    ResetCurveListStatus();
                    tempCurveDictionary = CurveMarksHelper.CurveUploadDictionary;
                }

                // Create serializable objects for each curve in the dictionary
                foreach (KeyValuePair<string, CurveMarksBO> kvp in tempCurveDictionary)
                {
                    key = String.Format("{0}-{1}-{2}", kvp.Value.CurveShortName, kvp.Value.CurveSet, kvp.Value.CurveStyle);

                    // If calling cell is now blank then skip this entry in the curve dictionary (User may have deleted formula)
                    string[] tokens = kvp.Key.Split('@');
                    string workbook = tokens[0];
                    string worksheetIndex = tokens[1];
                    string cell = tokens[2];

                    Excel.Application app = (Excel.Application)ExcelDnaUtil.Application;
                    worksheet = app.Worksheets[Convert.ToInt32(worksheetIndex)];
                    r2 = (Excel.Range)worksheet.get_Range(cell, cell);
                    obj = r2.Value2;
                    if (!isUploadMultipleMonthlyCurves)
                    {
                        if (obj == null || String.Compare(obj.ToString(), Global.RegisteredCurveMsgReady, false) != 0)
                            continue;
                    }

                    if (intraday && !kvp.Value.CurveSet.ToUpper().Equals("INTRADAY"))
                        continue;

                    CurveMark curveMark = CreateCurveMark(kvp);
                    if (curveMark != null)
                    {
                        if (curveMark.DateList.Count > 0 && curveMark.PriceList.Count > 0)
                        {
                            uploadCount++;
                            curveMarkList.Add(curveMark);
                        }
                        else if (curveMark.DateList.Count > 0 && curveMark.Coeff1List.Count > 0)
                        {
                            uploadCount++;
                            curveMarkList.Add(curveMark);
                        }
                    }                   
                }

                // If any of the curves are invalid then cancel the entire upload request
                if (uploadCount == -1)
                {
                    Global.DisplayMsg(String.Format("Curve {0} is Invalid.  Please check for blank dates or prices. \r\n\r\n No Curves Uploaded.", key), displayStatusMessageBox);
                }
                else if (uploadCount == 0)
                {
                    if (!intraday)
                        Global.DisplayMsg("No Curves to Upload!", displayStatusMessageBox);
                }
                else
                {
                    _log.Debug("Sending Curves at: " + System.DateTime.Now);
                    ((Excel.Application)(ExcelDnaUtil.Application)).ScreenUpdating = false;

                    Tuple<int, int> statusCount = SubmitCurveList(curveMarkList, displayStatusMessageBox);

                    ((Excel.Application)(ExcelDnaUtil.Application)).ScreenUpdating = true;

                    _log.Debug("Received Response at: " + System.DateTime.Now);
                    if (!intraday)
                    {
                        if (statusCount.Item2 > 0)
                        {
                            Global.DisplayMsg(String.Format("{0} Curve(s) Have Been Submitted." + Environment.NewLine
                                        + "Successfully uploaded: {1}" + Environment.NewLine
                                        + "Failed uploading: {2} " + Environment.NewLine,
                                        statusCount.Item1 + statusCount.Item2, statusCount.Item1, statusCount.Item2), true);
                            return false;
                        }
                        else
                        {
                            return true;
                        }


                    }
                }
            }
            catch (Exception e)
            {
                _log.Error("CurveUploadUtility::UploadCurveList() ", e);
                return false;
            }
            finally
            {
                CurveMarksHelper.ReleaseComObject(r2);
                CurveMarksHelper.ReleaseComObject(worksheet);

                ((Excel.Application)ExcelDnaUtil.Application).StatusBar = "Ready";
                ((Excel.Application)ExcelDnaUtil.Application).Cursor = Excel.XlMousePointer.xlDefault;
            }

            return true;
        }        

        public void ResetCurveListStatus()
        {
            CurveUploadUtility utility = new CurveUploadUtility();

            try
            {
                // Reset color and status in all cells registered for Curve Upload
                foreach (KeyValuePair<string, CurveMarksBO> kvp in CurveMarksHelper.CurveUploadDictionary)
                {
                    utility.ResetSingleCurveStatus(kvp.Key);
                }
            }
            catch (Exception ex)
            {
                _log.Error("CurveUploadUtility::ResetCurveListStatus ", ex);
            }
        }

        internal void RemoveEmptyCellCurves(IDictionary<string, CurveMarksBO> dict)
        {
            List<string> keys = new List<string>();
            Excel.Worksheet worksheet = null;
            Excel.Range r2 = null;
            Object obj = null;

            try
            {
                foreach (KeyValuePair<string, CurveMarksBO> kvp in dict)
                {
                    string[] tokens = kvp.Key.Split('@');
                    string workbook = tokens[0];
                    string worksheetIndex = tokens[1];
                    string cell = tokens[2];
                    Excel.Application app = (Excel.Application)ExcelDnaUtil.Application;
                    worksheet = app.Worksheets[Convert.ToInt32(worksheetIndex)];
                    r2 = (Excel.Range)worksheet.get_Range(cell, cell);
                    obj = r2.Value2;
                    if (!isUploadMultipleMonthlyCurves && !isDownloadMultipleMonthlyCurves)
                    {
                        if (obj == null || String.Compare(obj.ToString(), Global.RegisteredCurveMsgReady, false) != 0 && String.Compare(obj.ToString(), Global.RegisteredCurveDownloadMsg, false) != 0)
                        {
                            keys.Add(kvp.Key);
                        }
                    }

                    if (workbook != app.ActiveWorkbook.Name)
                    {
                        keys.Add(kvp.Key);
                    }
                }

                keys.ForEach(key =>
                {
                    if (key != null && dict.ContainsKey(key))
                    {
                        System.Diagnostics.Debug.Print("Removing " + key);
                        dict.Remove(key);
                    }
                });
            }
            catch (Exception ex)
            {
                _log.Error("CurveUploadUtility::RemoveEmptyCellCurves ", ex);
            }
            finally
            {
                CurveMarksHelper.ReleaseComObject(r2);
                CurveMarksHelper.ReleaseComObject(worksheet);
            }
        }

        private static bool IsDateCell(Excel.Range cell)
        {
            string format = cell.NumberFormat as string;

            if (format == null)
                return false;

            if (format.Contains("y"))
                return true;

            return false;
        }

        public static CurveMarksBO LoadPipelineCycleCurveMark(Range dateRange, Range valueRange, CurveMarksBO curveMarks)
        {


            int rowOffset = 0;
            List<string> datesList = new List<string>();
            List<string> pricesList = new List<string>();




            for (int row = 1; row <= dateRange.Rows.Count; row++)
            {
                DateTime rowDate = DateTime.FromOADate((double)dateRange.Cells[row, 1].Value2);
                DateTimeOffset rowDateWithOffset = new DateTimeOffset(rowDate, TimeZoneInfo.Local.GetUtcOffset(rowDate));

                string rowValue = Convert.ToString(valueRange.Cells[row, 1].Value2);

                datesList.Add(rowDateWithOffset.ToString("o"));
                pricesList.Add(rowValue);



                rowOffset++;

                if (row < dateRange.Rows.Count)
                {
                    DateTime nextDate = DateTime.FromOADate((double)dateRange.Cells[row + 1, 1].Value2);

                    rowDate = rowDate.AddDays(1);

                    while (rowDate < nextDate)
                    {
                        rowDateWithOffset = new DateTimeOffset(rowDate, TimeZoneInfo.Local.GetUtcOffset(rowDate));
                        datesList.Add(rowDateWithOffset.ToString("o"));
                        pricesList.Add(Convert.ToString(rowValue));


                        rowDate = rowDate.AddDays(1);
                        rowOffset++;
                        if (rowDate.Month > rowDate.AddDays(-1).Month)
                        {
                            break;
                        }

                    }

                }
            }

            //Fill Range past last row
            if (dateRange.Rows.Count > 1)
            {
                DateTime firstDate = DateTime.FromOADate((double)dateRange.Cells[1, 1].Value2);
                DateTime secondDate = DateTime.FromOADate((double)dateRange.Cells[2, 1].Value2);
                DateTime lastDate = DateTime.FromOADate((double)dateRange.Cells[dateRange.Rows.Count, 1].Value2);
                DateTimeOffset lastDateWithOffset = new DateTimeOffset(lastDate, TimeZoneInfo.Local.GetUtcOffset(lastDate));
                String lastValue = Convert.ToString(valueRange.Cells[dateRange.Rows.Count, 1].Value2);

                int daysDifference = (secondDate - firstDate).Days;

                for (int row = 0; row < daysDifference; row++)
                {

                    lastDate = lastDate.AddDays(1);
                    if (lastDate.Month > lastDate.AddDays(-1).Month)
                    {
                        break;
                    }
                    lastDateWithOffset = new DateTimeOffset(lastDate, TimeZoneInfo.Local.GetUtcOffset(lastDate));
                    datesList.Add(lastDateWithOffset.ToString("o"));
                    pricesList.Add(lastValue);



                    rowOffset++;
                }



            }

            if (rowOffset > 0)
            {
                curveMarks.IsPipelineCurveMark = true;
                curveMarks.PipelineCycleCurveDates = datesList;
                curveMarks.PipelineCycleCurveValues = pricesList;
            }


            return curveMarks;


        }

        private static CurveMark CreatePipelineCycleCurveMark(KeyValuePair<string, CurveMarksBO> kvp)
        {
            CurveMarksBO marks = kvp.Value;

            marks = LoadPipelineCycleCurveMark(marks.DateRange, marks.ValueRange, marks);

            CurveMark curveMark = new CurveMark
            {
                BlockName = marks.BlockName,
                ClientId = Global.ClientID,
                RequestId = kvp.Key,
                CurveMasterShortName = marks.CurveShortName,
                CurveSet = marks.CurveSet,
                CurveStyle = marks.CurveStyle,
                MarkDate = marks.MarkDate,
                Periodicity = marks.Periodicity,
                DateList = marks.PipelineCycleCurveDates,
                Date2List = new List<string>(),
                PriceList = marks.PipelineCycleCurveValues,
                Coeff1List = new List<string>(),
                Coeff2List = new List<string>(),
                Coeff3List = new List<string>(),
                Coeff4List = new List<string>(),
                Coeff5List = new List<string>(),
                Coeff6List = new List<string>(),                
                CreateUserId = Environment.UserName,
                ForwardCurveType = marks.ForwardCurveType,
                StrikePrice = marks.ValueStrike
            };

            return curveMark;
        }

        private static CurveMark CreateCurveMark(KeyValuePair<string, CurveMarksBO> kvp, bool displayMessageBox = true)
        {
            CurveMarksBO marks = kvp.Value;

            CultureInfo cultureInfo = CultureInfo.CurrentUICulture;

            if (marks.IsPipelineCurveMark)
                return CreatePipelineCycleCurveMark(kvp);

            CurveMark curveMark = new CurveMark
            {
                ContextID = marks.ContextID,              
                CurveMasterShortName = marks.CurveShortName,
                BlockName = marks.BlockName,
                ClientId = Global.ClientID,
                RequestId = kvp.Key,
                CurveSet = marks.CurveSet,
                CurveStyle = marks.CurveStyle,
                MarkDate = marks.MarkDate,
                Periodicity = marks.Periodicity,
                DateList = new List<string>(),
                Date2List = new List<string>(),
                PriceList = new List<string>(),
                Coeff1List = new List<string>(),
                Coeff2List = new List<string>(),
                Coeff3List = new List<string>(),
                Coeff4List = new List<string>(),
                Coeff5List = new List<string>(),
                Coeff6List = new List<string>(),
                Coeff7List = new List<string>(),
                CreateUserId = Environment.UserName,
                ForwardCurveType = marks.ForwardCurveType,
                StrikePrice = marks.ValueStrike                
            };

            CurveMarkPoint curveMarkPoint = new CurveMarkPoint();


            int rowIndex = 0;
            bool isDateOverride = false;


            foreach (Excel.Range row in marks.DateRange.Rows)
            {
                rowIndex++;

                Excel.Range cell = (Excel.Range)row.Cells[1, 1];
                if (cell.Value2 != null && String.Compare(cell.Text.ToString(), "#N/A", false) != 0)
                {
                    if (isDateOverride == false)
                    {
                        bool isDate = IsDateCell(cell);

                        if (!isDate)
                        {
                            if (displayMessageBox)
                            {
                                DialogResult result = MessageBox.Show(String.Format("Date Range for {0} does not look like a valid date. Continue?", marks.CurveShortName),
                                    Global.MessageBoxTitle, MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                                if (result == DialogResult.No)
                                    return null;
                                else
                                    isDateOverride = true;
                            }
                            else
                            {
                                _log.Error("Date Range for {0} does not look like a valid date. Exiting Curve Upload Process without submitting Curves.");
                                return null;

                            }
                        }
                    }

                    DateTime dt = DateTime.FromOADate((double)cell.Value2);
                    DateTimeOffset dtOffset = new DateTimeOffset(dt,
                            TimeZoneInfo.Local.GetUtcOffset(dt));

                    curveMarkPoint.Date = dtOffset.ToString("o");
                    //curveMark.DateList.Add(dtOffset.ToString("o"));

                }
                else
                    return null;
                // if this is a Spread Vol, interpret the 2nd date
                if (marks.ForwardCurveType.Equals(ForwardCurveType.SPREADVOL.ToString()))
                {
                    cell = (Excel.Range)row.Cells[1, 2];
                }
                else if (marks.ForwardCurveType.Equals(ForwardCurveType.POLYVOL.ToString()) || marks.ForwardCurveType.Equals(ForwardCurveType.VOLATILITY.ToString()))
                {
                    cell = (Excel.Range)row.Cells[1, 1];
                }
                if (cell.Value2 != null && String.Compare(cell.Text.ToString(), "#N/A", false) != 0)
                {
                    if (isDateOverride == false)
                    {
                        bool isDate = IsDateCell(cell);

                        if (!isDate)
                        {
                            if (displayMessageBox)
                            {
                                DialogResult result = MessageBox.Show(String.Format("Date Range for {0} does not look like a valid date. Continue?", marks.CurveShortName),
                                    Global.MessageBoxTitle, MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                                if (result == DialogResult.No)
                                    return null;
                                else
                                    isDateOverride = true;
                            }
                            else
                            {
                                _log.Error("Date Range for {0} does not look like a valid date. Exiting Curve Upload Process without submitting Curves.");
                                return null;

                            }
                        }
                    }

                    DateTime dt = DateTime.FromOADate((double)cell.Value2);
                    DateTimeOffset dtOoffset = new DateTimeOffset(dt,
                            TimeZoneInfo.Local.GetUtcOffset(dt));
                    if (marks.ForwardCurveType.Equals(ForwardCurveType.SPREADVOL.ToString()))
                    {
                        curveMarkPoint.Date2 = dtOoffset.ToString("o");
                        //curveMark.Date2List.Add(dtOoffset.ToString("o"));
                    }
                }
                else
                {
                    string nullAddress = cell.get_Address();
                    _log.Error("Curve has null value at " + nullAddress);
                    return null;
                }

                // if this is a Poly Vol (polynomial)
                if (marks.ForwardCurveType.Equals(ForwardCurveType.POLYVOL.ToString()))
                {
                    // get the atm vol
                    cell = (Excel.Range)marks.AtmVolRange[rowIndex, 1];
                    cell = (Excel.Range)marks.AtmVolRange[rowIndex, 1];
                    if (cell.Value2 != null)
                    {
                        double dummyOut = new double();
                        bool result = Double.TryParse(cell.Text.ToString(), NumberStyles.Any, cultureInfo.NumberFormat, out dummyOut);

                        if (result)
                        {
                            curveMarkPoint.Price = cell.Value2.ToString();
                            //curveMark.PriceList.Add(cell.Value2.ToString());
                        }
                        else
                            continue;
                    }
                    // get the coefficients
                    for (int k = 1; k <= ((Excel.Range)marks.ValueRange).Columns.Count; k++)
                    {
                        cell = (Excel.Range)marks.ValueRange[rowIndex, k];
                        if (cell.Value2 != null)
                        {
                            double dummyOut = new double();
                            bool result = Double.TryParse(cell.Text.ToString(), NumberStyles.Any, cultureInfo.NumberFormat, out dummyOut);

                            if (result)
                            {
                                if (k == 1)
                                {
                                    curveMarkPoint.Coeff1 = cell.Value2.ToString();
                                    //curveMark.Coeff1List.Add(cell.Value2.ToString());
                                }
                                if (k == 2)
                                {
                                    curveMarkPoint.Coeff2 = cell.Value2.ToString();
                                    //curveMark.Coeff2List.Add(cell.Value2.ToString());
                                }
                                if (k == 3)
                                {
                                    curveMarkPoint.Coeff3 = cell.Value2.ToString();
                                    //curveMark.Coeff3List.Add(cell.Value2.ToString());
                                }
                                if (k == 4)
                                {
                                    curveMarkPoint.Coeff4 = cell.Value2.ToString();
                                    //curveMark.Coeff4List.Add(cell.Value2.ToString());
                                }
                            }
                            else
                                continue;
                        }
                        else
                        {
                            string nullAddress = cell.get_Address();
                            _log.Error("Curve has null value at " + nullAddress);
                            return null;
                        }
                    }
                }
                else if (marks.ForwardCurveType.Equals(ForwardCurveType.VOLS.ToString()))
                {                    
                    // get the coefficients
                    for (int k = 1; k <= ((Excel.Range)marks.ValueRange).Columns.Count; k++)
                    {
                        cell = (Excel.Range)marks.ValueRange[rowIndex, k];
                        if (cell.Value2 != null)
                        {
                            double dummyOut = new double();
                            bool result = Double.TryParse(cell.Text.ToString(), NumberStyles.Any, cultureInfo.NumberFormat, out dummyOut);

                            if (result)
                            {
                                if (k == 1)
                                {
                                    curveMarkPoint.Coeff1 = cell.Value2.ToString();
                                    //curveMark.Coeff1List.Add(cell.Value2.ToString());
                                }
                                if (k == 2)
                                {
                                    curveMarkPoint.Coeff2 = cell.Value2.ToString();
                                    //curveMark.Coeff2List.Add(cell.Value2.ToString());
                                }
                                if (k == 3)
                                {
                                    curveMarkPoint.Coeff3 = cell.Value2.ToString();
                                    //curveMark.Coeff3List.Add(cell.Value2.ToString());
                                }
                                if (k == 4)
                                {
                                    curveMarkPoint.Coeff4 = cell.Value2.ToString();
                                    //curveMark.Coeff4List.Add(cell.Value2.ToString());
                                }
                                if (k == 5)
                                {
                                    curveMarkPoint.Coeff5 = cell.Value2.ToString();
                                    //curveMark.Coeff4List.Add(cell.Value2.ToString());
                                }
                                if (k == 6)
                                {
                                    curveMarkPoint.Coeff6 = cell.Value2.ToString();
                                    //curveMark.Coeff4List.Add(cell.Value2.ToString());
                                }
                                if (k == 7)
                                {
                                    curveMarkPoint.Coeff7 = cell.Value2.ToString();
                                    //curveMark.Coeff4List.Add(cell.Value2.ToString());
                                }
                            }
                            else
                                continue;
                        }
                        else
                        {
                            string nullAddress = cell.get_Address();
                            _log.Error("Curve has null value at " + nullAddress);
                            return null;
                        }
                    }
                }
                else
                {
                    cell = (Excel.Range)marks.ValueRange[rowIndex, 1];
                    if (cell.Value2 != null)
                    {
                        double dummyOut = new double();
                        bool result = Double.TryParse(cell.Text.ToString(), NumberStyles.Any, cultureInfo.NumberFormat, out dummyOut);

                        if (result)
                        {
                            curveMarkPoint.Price = cell.Value2.ToString();
                            //curveMark.PriceList.Add(cell.Value2.ToString());
                        }
                        else
                            continue;
                    }
                    else
                    {
                        if (marks.ForwardCurveType.Equals(ForwardCurveType.VOLATILITY.ToString()))
                            continue;
                        {
                            string nullAddress = cell.get_Address();
                            _log.Error("Curve has null value at " + nullAddress);

                            return null;
                        }
                    }
                }

                //If we are here, we can add the curveMarkPoint values to curveMark
                curveMark = AddCurveMarkPointToCurveMark(curveMarkPoint, curveMark);
            }


            return curveMark;


        }

        private static CurveMark AddCurveMarkPointToCurveMark(CurveMarkPoint curveMarkPoint, CurveMark curveMark)
        {
            if (!String.IsNullOrEmpty(curveMarkPoint.Date))
                curveMark.DateList.Add(curveMarkPoint.Date);

            if (!String.IsNullOrEmpty(curveMarkPoint.Price))
                curveMark.PriceList.Add(curveMarkPoint.Price);

            if (!String.IsNullOrEmpty(curveMarkPoint.Date2))
                curveMark.Date2List.Add(curveMarkPoint.Date2);

            if (!String.IsNullOrEmpty(curveMarkPoint.Coeff1))
                curveMark.Coeff1List.Add(curveMarkPoint.Coeff1);

            if (!String.IsNullOrEmpty(curveMarkPoint.Coeff2))
                curveMark.Coeff2List.Add(curveMarkPoint.Coeff2);

            if (!String.IsNullOrEmpty(curveMarkPoint.Coeff3))
                curveMark.Coeff3List.Add(curveMarkPoint.Coeff3);

            if (!String.IsNullOrEmpty(curveMarkPoint.Coeff4))
                curveMark.Coeff4List.Add(curveMarkPoint.Coeff4);

            if (!String.IsNullOrEmpty(curveMarkPoint.Coeff5))
                curveMark.Coeff5List.Add(curveMarkPoint.Coeff5);

            if (!String.IsNullOrEmpty(curveMarkPoint.Coeff6))
                curveMark.Coeff6List.Add(curveMarkPoint.Coeff6);

            if (!String.IsNullOrEmpty(curveMarkPoint.Coeff7))
                curveMark.Coeff7List.Add(curveMarkPoint.Coeff7);

            return curveMark;

        }        

        /// <summary>
        /// Request curves to download
        /// </summary>
        internal void DownloadCurveList(string singleCurve)
        {
            IDictionary<string, CurveMarksBO> tempCurveDictionary;

            Excel.Worksheet worksheet = null;
            Excel.Range r2 = null;
            object obj = null;

            ((Excel.Application)ExcelDnaUtil.Application).StatusBar = "Downloading curves...";
            ((Excel.Application)ExcelDnaUtil.Application).Cursor = Excel.XlMousePointer.xlWait;

            RemoveEmptyCellCurves(CurveMarksHelper.CurveDownloadDictionary);

            try
            {
                // Determine if uploading single curve or entire list
                if (singleCurve != null)
                {
                    tempCurveDictionary = new Dictionary<string, CurveMarksBO>();
                    string[] tokens = singleCurve.Split('$');
                    string singleCurveKey = tokens[tokens.Length - 1];
                    if (CurveMarksHelper.CurveDownloadDictionary.ContainsKey(singleCurveKey))
                    {
                        tempCurveDictionary.Add(singleCurveKey, CurveMarksHelper.CurveDownloadDictionary[singleCurveKey]);
                    }
                }
                else
                {
                    tempCurveDictionary = CurveMarksHelper.CurveDownloadDictionary;
                }

                List<DownloadCurveParams> paramsList = new List<DownloadCurveParams>();

                // Create serializable objects for each curve in the dictionary
                foreach (KeyValuePair<string, CurveMarksBO> kvp in tempCurveDictionary)
                {
                    string[] tokens = kvp.Key.Split('@');
                    string workbook = tokens[0];
                    string worksheetIndex = tokens[1];
                    string caller = tokens[2];
                    string loc = kvp.Value.DownloadCell.get_Address(kvp.Value.DownloadCell.Row, kvp.Value.DownloadCell.Column, Excel.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
                    Excel.Application app = (Excel.Application)ExcelDnaUtil.Application;
                    worksheet = app.Worksheets[Convert.ToInt32(worksheetIndex)];
                    r2 = (Excel.Range)worksheet.get_Range(caller, caller);
                    obj = r2.Value2;
                    if (!isDownloadMultipleMonthlyCurves)
                    {
                        if (obj == null || String.Compare(obj.ToString(), Global.RegisteredCurveDownloadMsg, false) != 0)
                            continue;
                    }

                    DownloadCurveParams reqParams = new DownloadCurveParams();

                    reqParams.ShortName = kvp.Value.CurveShortName;
                    //reqParams.RequestId = kvp.Key;
                    reqParams.RequestId = String.Format("{0}@{1}@{2}", tokens[0], worksheetIndex, loc.Replace("$", String.Empty));



                    if (kvp.Value.ForwardCurveType != null)
                    {
                        reqParams.ForwardCurveType = kvp.Value.ForwardCurveType;
                    }

                    if (kvp.Value.MarkDate != null)
                    {
                        reqParams.MarkDate = kvp.Value.MarkDate;
                    }

                    if (kvp.Value.CurveSet != null)
                    {
                        reqParams.CurveSet = kvp.Value.CurveSet;
                    }

                    if (kvp.Value.CurveStyle != null)
                    {
                        reqParams.CurveStyle = kvp.Value.CurveStyle;
                    }

                    if (kvp.Value.BlockName != null)
                    {
                        reqParams.BlockName = kvp.Value.BlockName;
                    }

                    if (kvp.Value.StartDate != null)
                    {
                        reqParams.StartDate = kvp.Value.StartDate;
                    }

                    if (kvp.Value.EndDate != null)
                    {
                        reqParams.EndDate = kvp.Value.EndDate;
                    }

                    if (kvp.Value.ValueStrike != null)
                    {
                        reqParams.StrikePrice = kvp.Value.ValueStrike;
                    }

                    if (kvp.Value.ContextID != null)
                        reqParams.ContextID = kvp.Value.ContextID;

                    paramsList.Add(reqParams);

                }

                CurveGateway gateway = new CurveGateway(Global.CurveWebServiceUrl);

                List<DownloadCurveParams> downloadedCurvesList = gateway.DownloadCurves(paramsList);

                foreach (var item in downloadedCurvesList)
                {
                    DisplayDownloadCurveHandler(item);
                }
            }
            catch (Exception e)
            {
                _log.Error("CurveUploadUtility::DownloadCurveList", e);
            }
            finally
            {

                ((Excel.Application)ExcelDnaUtil.Application).StatusBar = "";
                ((Excel.Application)ExcelDnaUtil.Application).Cursor = Excel.XlMousePointer.xlDefault;

                // Release COM object resources
                CurveMarksHelper.ReleaseComObject(r2);
                CurveMarksHelper.ReleaseComObject(worksheet);
            }
        }

        /// <summary>
        /// Request curves to download
        /// </summary>
        internal void DownloadCurveList(string singleCurve, Excel.Range outputDateRange)
        {
            IDictionary<string, CurveMarksBO> tempCurveDictionary;

            Excel.Worksheet worksheet = null;
            Excel.Range r2 = null;
            object obj = null;

            ((Excel.Application)ExcelDnaUtil.Application).StatusBar = "Downloading curves...";
            ((Excel.Application)ExcelDnaUtil.Application).Cursor = Excel.XlMousePointer.xlWait;

            RemoveEmptyCellCurves(CurveMarksHelper.CurveDownloadDictionary);

            try
            {
                // Determine if uploading single curve or entire list
                if (singleCurve != null)
                {
                    tempCurveDictionary = new Dictionary<string, CurveMarksBO>();
                    string[] tokens = singleCurve.Split('$');
                    string singleCurveKey = tokens[tokens.Length - 1];
                    if (CurveMarksHelper.CurveDownloadDictionary.ContainsKey(singleCurveKey))
                    {
                        tempCurveDictionary.Add(singleCurveKey, CurveMarksHelper.CurveDownloadDictionary[singleCurveKey]);
                    }
                }
                else
                {
                    tempCurveDictionary = CurveMarksHelper.CurveDownloadDictionary;
                }

                List<DownloadCurveParams> paramsList = new List<DownloadCurveParams>();

                // Create serializable objects for each curve in the dictionary
                foreach (KeyValuePair<string, CurveMarksBO> kvp in tempCurveDictionary)
                {
                    string[] tokens = kvp.Key.Split('@');
                    string workbook = tokens[0];
                    string worksheetIndex = tokens[1];
                    string caller = tokens[2];
                    string loc = kvp.Value.DownloadCell.get_Address(kvp.Value.DownloadCell.Row, kvp.Value.DownloadCell.Column, Excel.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
                    Excel.Application app = (Excel.Application)ExcelDnaUtil.Application;
                    worksheet = app.Worksheets[Convert.ToInt32(worksheetIndex)];
                    r2 = (Excel.Range)worksheet.get_Range(caller, caller);
                    obj = r2.Value2;
                    if (!isDownloadMultipleMonthlyCurves)
                    {
                        if (obj == null || String.Compare(obj.ToString(), Global.RegisteredCurveDownloadMsg, false) != 0)
                            continue;
                    }

                    DownloadCurveParams reqParams = new DownloadCurveParams();

                    reqParams.ShortName = kvp.Value.CurveShortName;
                    //reqParams.RequestId = kvp.Key;
                    reqParams.RequestId = String.Format("{0}@{1}@{2}", tokens[0], worksheetIndex, loc.Replace("$", String.Empty));



                    if (kvp.Value.ForwardCurveType != null)
                    {
                        reqParams.ForwardCurveType = kvp.Value.ForwardCurveType;
                    }

                    if (kvp.Value.MarkDate != null)
                    {
                        reqParams.MarkDate = kvp.Value.MarkDate;
                    }

                    if (kvp.Value.CurveSet != null)
                    {
                        reqParams.CurveSet = kvp.Value.CurveSet;
                    }

                    if (kvp.Value.CurveStyle != null)
                    {
                        reqParams.CurveStyle = kvp.Value.CurveStyle;
                    }

                    if (kvp.Value.BlockName != null)
                    {
                        reqParams.BlockName = kvp.Value.BlockName;
                    }

                    if (kvp.Value.StartDate != null)
                    {
                        reqParams.StartDate = kvp.Value.StartDate;
                    }

                    if (kvp.Value.EndDate != null)
                    {
                        reqParams.EndDate = kvp.Value.EndDate;
                    }

                    if (kvp.Value.ValueStrike != null)
                    {
                        reqParams.StrikePrice = kvp.Value.ValueStrike;
                    }

                    if (kvp.Value.ContextID != null)
                        reqParams.ContextID = kvp.Value.ContextID;

                    StackFrame stackFrame = new StackFrame(1);
                    if (stackFrame != null)
                    {
                        var method = stackFrame.GetMethod();
                        if (method != null)
                        {
                            reqParams.FunctionName = method.Name;
                        }
                    }

                    paramsList.Add(reqParams);

                }

                CurveGateway gateway = new CurveGateway(Global.CurveWebServiceUrl);

                List<DownloadCurveParams> downloadedCurvesList = gateway.DownloadCurves(paramsList);
                List<DateTime> dateList = new List<DateTime>();

                for (int i = 1; i <= outputDateRange.Count; i++)
                {
                    DateTime b = DateTime.FromOADate((double)outputDateRange.Value2[i, 1]);
                    //b = new DateTime(b.Year, b.Month, b.Day,b.Minute, b.Second, b.Millisecond, DateTimeKind.Local);
                    dateList.Add(b);
                }

                foreach (var item in downloadedCurvesList)
                {
                    DisplayDownloadCurveHandler(item, dateList);
                }
            }
            catch (Exception e)
            {
                _log.Error("CurveUploadUtility::DownloadCurveList", e);
            }
            finally
            {

                ((Excel.Application)ExcelDnaUtil.Application).StatusBar = "";
                ((Excel.Application)ExcelDnaUtil.Application).Cursor = Excel.XlMousePointer.xlDefault;

                // Release COM object resources
                CurveMarksHelper.ReleaseComObject(r2);
                CurveMarksHelper.ReleaseComObject(worksheet);
            }
        }

        /// <summary>
        /// Display downloaded curves
        /// </summary>
        /// <param name="marks"></param>
        public void DisplayDownloadCurveHandler(DownloadCurveParams curveMark, List<DateTime> outputDateList)
        {

            Excel.Worksheet sh = null;
            Excel.Range cell = null;
            Excel.Range cell1 = null;
            Excel.Range cell2 = null;
            Excel.Range cell3 = null;
            Excel.Range cell4 = null;
            Excel.Range cell5 = null;
            Excel.Range cell6 = null;
            Excel.Range cell7 = null;

            ((Excel.Application)ExcelDnaUtil.Application).ScreenUpdating = false;

            try
            {
                if (curveMark.CurveData != null)
                {
                    string key = curveMark.RequestId;
                    string[] tokens = key.Split('@');
                    string workbook = tokens[0];
                    string worksheetIndex = tokens[1];
                    sh = (Excel.Worksheet)((Excel.Application)ExcelDnaUtil.Application).Application.Worksheets[Convert.ToInt32(worksheetIndex)];
                    cell = (Excel.Range)sh.get_Range(tokens[2], tokens[2]);

                    Curve curve = ParseCurveData(curveMark);

                    Dictionary<string, string> datePriceDictionary = new Dictionary<string, string>();

                    for (int i = 0; i < outputDateList.Count; i++)
                    {
                        datePriceDictionary.Add(outputDateList[i].ToString(), "-");


                        foreach (var item in curve.DateList)
                        {
                            DateTime dt = Convert.ToDateTime(item);
                            if (dt == outputDateList[i])
                            {
                                datePriceDictionary[outputDateList[i].ToString()] = curve.ValueList[curve.DateList.IndexOf(item)];
                                break;
                            }
                        }
                    }

                    // setup dataGrid, based on curve type
                    object[,] dataGrid;
                    int rows = datePriceDictionary.Keys.Count;
                    dataGrid = new object[rows, 1];
                    int r = 0;
                    foreach (var item in datePriceDictionary.Keys)
                    {
                        dataGrid[r, 0] = datePriceDictionary[item];
                        r++;
                    }

                    Excel.Range range = ((Excel.Range)sh.Cells[cell.Row, cell.Column]).get_Resize(rows, 1);
                    range.Value = dataGrid;
                }
            }
            catch (Exception ex)
            {
                _log.Error("CurveUploadUtility::DisplayDownloadCurveHandler: ", ex);
            }
            finally
            {
                ((Excel.Application)ExcelDnaUtil.Application).ScreenUpdating = true;
                // Release COM object resources
                CurveMarksHelper.ReleaseComObject(cell);
                CurveMarksHelper.ReleaseComObject(cell1);
                CurveMarksHelper.ReleaseComObject(cell2);
                CurveMarksHelper.ReleaseComObject(cell3);
                CurveMarksHelper.ReleaseComObject(cell4);
                CurveMarksHelper.ReleaseComObject(cell5);
                CurveMarksHelper.ReleaseComObject(cell6);
                CurveMarksHelper.ReleaseComObject(cell7);
                CurveMarksHelper.ReleaseComObject(sh);
            }
        }

        /// <summary>
        /// Display downloaded curves
        /// </summary>
        /// <param name="marks"></param>
        public void DisplayDownloadCurveHandler(DownloadCurveParams curveMark)
        {
            Excel.Worksheet sh = null;
            Excel.Range cell = null;
            Excel.Range cell1 = null;
            Excel.Range cell2 = null;
            Excel.Range cell3 = null;
            Excel.Range cell4 = null;
            Excel.Range cell5 = null;
            Excel.Range cell6 = null;
            Excel.Range cell7 = null;

            ((Excel.Application)ExcelDnaUtil.Application).ScreenUpdating = false;

            try
            {
                if (curveMark.CurveData != null)
                {
                    string key = curveMark.RequestId; //curveMarks.CURVEMARK[0].REQUESTID;
                    string[] tokens = key.Split('@');
                    string workbook = tokens[0];
                    string worksheetIndex = tokens[1];
                    sh = (Excel.Worksheet)((Excel.Application)ExcelDnaUtil.Application).Application.Worksheets[Convert.ToInt32(worksheetIndex)];
                    cell = (Excel.Range)sh.get_Range(tokens[2], tokens[2]);


                    Curve curve = ParseCurveData(curveMark);



                    int rows = curve.DateList.Count;


                    cell1 = (Excel.Range)sh.Cells[cell.Row, cell.Column];
                    cell2 = (Excel.Range)sh.Cells[cell.Row, cell.Column];
                    cell4 = sh.get_Range(cell1, cell2);
                    cell4.Value2 = "Curve: " + curve.Name;
                    cell1 = (Excel.Range)sh.Cells[cell.Row + 1, cell.Column];
                    cell2 = (Excel.Range)sh.Cells[cell.Row + 1, cell.Column];
                    cell4 = sh.get_Range(cell1, cell2);
                    cell4.Value2 = "Set: " + curve.CurveSet;
                    cell1 = (Excel.Range)sh.Cells[cell.Row + 2, cell.Column];
                    cell2 = (Excel.Range)sh.Cells[cell.Row + 2, cell.Column];
                    cell4 = sh.get_Range(cell1, cell2);
                    cell4.Value2 = "Style: " + curve.CurveStyle;
                    cell1 = (Excel.Range)sh.Cells[cell.Row + 3, cell.Column];
                    cell2 = (Excel.Range)sh.Cells[cell.Row + 3, cell.Column];
                    DateTime markDate = DateTime.Parse(curve.MarkDate);
                    cell4 = sh.get_Range(cell1, cell2);
                    cell4.Value2 = "Date: " + markDate.ToShortDateString();
                    cell1 = (Excel.Range)sh.Cells[cell.Row + 4, cell.Column];
                    cell2 = (Excel.Range)sh.Cells[cell.Row + 4, cell.Column];
                    cell4 = sh.get_Range(cell1, cell2);
                    cell4.Value2 = "Context ID: " + curve.ContextID;
                    cell1 = (Excel.Range)sh.Cells[cell.Row + 6, cell.Column];
                    cell2 = (Excel.Range)sh.Cells[cell.Row + 6, cell.Column];
                    cell6 = sh.get_Range(cell1, cell2);
                    //cell5.Value2 = "Type: " + pts.TYPE;

                    // if curve type is volatility or spread then display strike
                    cell1 = (Excel.Range)sh.Cells[cell.Row + 7, cell.Column];
                    cell2 = (Excel.Range)sh.Cells[cell.Row + 7, cell.Column];
                    cell7 = sh.get_Range(cell1, cell2);


                    // setup dataGrid, based on curve type
                    object[,] dataGrid;

                    dataGrid = new object[rows, 2];

                    for (int r = 0; r < rows; r++)
                    {
                        DateTime vDate = DateTime.Parse(curve.DateList[r]);
                        dataGrid[r, 0] = vDate;
                        dataGrid[r, 1] = curve.ValueList[r];
                    }


                    Excel.Range range = ((Excel.Range)sh.Cells[cell.Row + 7, cell.Column]).get_Resize(rows, 2);
                    range.Value = dataGrid;


                }
            }
            catch (Exception ex)
            {
                _log.Error("CurveUploadUtility::DisplayDownloadCurveHandler: ", ex);
            }
            finally
            {
                ((Excel.Application)ExcelDnaUtil.Application).ScreenUpdating = true;
                // Release COM object resources
                CurveMarksHelper.ReleaseComObject(cell);
                CurveMarksHelper.ReleaseComObject(cell1);
                CurveMarksHelper.ReleaseComObject(cell2);
                CurveMarksHelper.ReleaseComObject(cell3);
                CurveMarksHelper.ReleaseComObject(cell4);
                CurveMarksHelper.ReleaseComObject(cell5);
                CurveMarksHelper.ReleaseComObject(cell6);
                CurveMarksHelper.ReleaseComObject(cell7);
                CurveMarksHelper.ReleaseComObject(sh);
            }
        }

        private Curve ParseCurveData(DownloadCurveParams dcp)
        {
            Curve curve = new Curve();

            String curveData = dcp.CurveData;

            String[] curveDataArray = curveData.Split(new char[] { ';' });

            if (curveDataArray.Length == 0)
                return null;

            String curveHeader = curveDataArray[0];

            foreach (String header in curveHeader.Split(new char[] { ',' }))
            {
                if (header.Split(new char[] { '=' }).Length > 0)
                {
                    String key = header.Split(new char[] { '=' })[0];
                    String value = header.Split(new char[] { '=' })[1];

                    if (key.ToUpper() == "SHORTNAME")
                        curve.Name = value;
                    if (key.ToUpper() == "CURVESET")
                        curve.CurveSet = value;
                    if (key.ToUpper() == "CURVESTYLENAME")
                        curve.CurveStyle = value;
                    if (key.ToUpper() == "MARKDATE")
                        curve.MarkDate = value;
                    if (key.ToUpper() == "CONTEXTID")
                        curve.ContextID = value;
                }
            }

            if (curveDataArray.Length == 1)
                return curve;


            List<String> dateList = new List<string>();
            List<String> valueList = new List<string>();

            //foreach (String curvePoint in curveDataArray)
            for (int i = 1; i < curveDataArray.Length; i++)
            {
                String curvePoint = curveDataArray[i];
                if (curvePoint.Split(new char[] { ',' }).Length == 2)
                {
                    String dateDef = curvePoint.Split(new char[] { ',' })[0];
                    String valueDef = curvePoint.Split(new char[] { ',' })[1];
                    if (dateDef.Contains("=") && valueDef.Contains("="))
                    {
                        dateList.Add(dateDef.Split(new char[] { '=' })[1]);
                        valueList.Add(valueDef.Split(new char[] { '=' })[1]);
                    }
                }
            }

            curve.DateList = dateList;
            curve.ValueList = valueList;

            return curve;




        }

        /// <summary>
        /// Fetches the excel values from the given range.
        /// </summary>
        /// <param name="dateRange">the range in which date values are passed.</param>
        /// <param name="valueRange">the range in which other values are passed.</param>
        public List<CurveMarksBO> GetCurveUploadValuesFromExcelRange(Excel.Range dateRange, Excel.Range valueRange)
        {
            Excel.Worksheet worksheet = null;
            Excel.Range startCell = null;
            Excel.Range endCell = null;
            Excel.Range splittedValueRange = null;
            try
            {
                List<CurveMarksBO> curveMarksBOList = new List<CurveMarksBO>();
                // columnCount gets the count of the selected columns in the given excel value range.
                int columnCount = valueRange.Columns.Count;
                // rowCount gets the count of the selected rows in the given excel value range.
                int rowCount = valueRange.Rows.Count;

                for (int column = 1; column <= columnCount; column++)
                {
                    string curveName = valueRange.Value2[1, column] as string;
                    string curveSet = valueRange.Value2[2, column] as string;
                    string contextID = valueRange.Value2[3, column] as string;
                    string periodicity = valueRange.Value2[4, column] as string;
                    string curveStyle = valueRange.Value2[5, column] as string;
                    worksheet = (Excel.Worksheet)dateRange.Worksheet;
                    startCell = (Range)worksheet.Cells[6, column];
                    endCell = (Range)worksheet.Cells[rowCount, column];
                    // Get the excel range for value cells corresponding to the date cells
                    splittedValueRange = valueRange.Range[startCell, endCell];
                    for (int i = 1; i <= splittedValueRange.Value2.Length; i++)
                    {
                        if (splittedValueRange.Value2[i, 1] == null)
                        {
                            // set the value as - if the range value is null.// Also writes the same in the excel.
                            splittedValueRange.Cells.set_Item(i, 1, "-");
                        }
                        // aligning the cell values as center aligned.
                        splittedValueRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;
                    }
                    CurveMarksBO curveMarksBO = new CurveMarksBO
                    {
                        DateRange = dateRange,
                        ValueRange = splittedValueRange,
                        CurveShortName = curveName,
                        CurveSet = curveSet,
                        CurveStyle = curveStyle,
                        BlockName = String.Empty,
                        Periodicity = periodicity,
                        ForwardCurveType = ForwardCurveType.PRICE.ToString(),
                        CurveStatus = Global.CurveUploadStatusPending,
                        CurveStatusMsg = "Curve Not Uploaded",
                        CurveStatusDateTime = DateTime.Now,
                        MarkDate = Global.MarkDate ?? DateTime.Now.Date.ToString("o"),
                        ContextID = contextID
                    };
                    // Add the curve marks Bo object into a list.
                    curveMarksBOList.Add(curveMarksBO);
                }
                return curveMarksBOList;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// This method fetches the parameters to download the curve.
        /// </summary>
        /// <param name="markDate">the as of date</param>
        /// <param name="valueRange">the range of parameters passed</param>
        /// <param name="downloadCell">the cell from where the output is to be displayed</param>
        /// <returns>the CurveMarksBO object list</returns>
        public List<CurveMarksBO> GetDownloadCurveParameterFromExcelRange(DateTime markDate, Excel.Range valueRange, Excel.Range downloadCell)
        {
            Excel.Worksheet worksheet = null;
            Excel.Range startCell = null;
            try
            {
                List<CurveMarksBO> curveMarksBOList = new List<CurveMarksBO>();
                // columnCount gets the count of the selected columns in the given excel value range.
                int columnCount = valueRange.Columns.Count;

                for (int column = 1; column <= columnCount; column++)
                {
                    string curveName = valueRange.Value2[1, column] as string;
                    string curveSet = valueRange.Value2[2, column] as string;
                    string contextID = valueRange.Value2[3, column] as string;
                    CurveMarksBO curveMarksBO = new CurveMarksBO
                    {
                        DownloadCell = downloadCell,
                        CurveShortName = curveName,
                        MarkDate = markDate.ToString("d"),
                        CurveSet = curveSet,
                        ContextID = contextID
                    };

                    // Add the curve marks Bo object into a list.
                    curveMarksBOList.Add(curveMarksBO);
                    worksheet = (Excel.Worksheet)downloadCell.Worksheet;
                    startCell = (Range)worksheet.Cells[downloadCell.Row, downloadCell.Column + 1];
                    // Assign the new cell for writing the next curve's value.
                    downloadCell = startCell;
                }
                return curveMarksBOList;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }

        /// <summary>
        /// Fetches the excel values from the given range.
        /// </summary>
        /// <param name="dateRange">the range in which date values are passed.</param>
        /// <param name="valueRange">the range in which other values are passed.</param>
        public List<CurveMarksBO> GetCurveUploadValuesFromExcelRangeForVols(Excel.Range dateRange, Excel.Range curveDefRange, Excel.Range valueRange, string markDateRange)
        {             
            try
            {
                List<CurveMarksBO> curveMarksBOList = new List<CurveMarksBO>();
                
                // columnCount gets the count of the selected columns in the given excel value range.
                int columnCount = valueRange.Columns.Count;
                // rowCount gets the count of the selected rows in the given excel value range.
                int rowCount = valueRange.Rows.Count;

                string curveName = curveDefRange.Value2[1, 1] as string;
                string curveSet = curveDefRange.Value2[2, 1] as string;
                string contextID = curveDefRange.Value2[3, 1] as string;
                string periodicity = curveDefRange.Value2[4, 1] as string;
                string curveStyle = curveDefRange.Value2[5, 1] as string;
                string curverMarkDate = DateTime.Now.Date.ToString("o");

                if (markDateRange != null && markDateRange != string.Empty)
                {
                    curverMarkDate = Convert.ToDateTime(markDateRange).ToString("o");
                }                
               
                for (int j = 1; j <= columnCount; j++)
                {
                    for (int i = 1; i <= (valueRange.Value2.Length / columnCount); i++)
                    {
                        if (valueRange.Value2[i, j] == null)
                        {
                            // set the value as - if the range value is null.// Also writes the same in the excel.
                            valueRange.Cells.set_Item(i, 1, "-");
                        }
                    }
                }                    

                // aligning the cell values as center aligned.
                valueRange.HorizontalAlignment = Microsoft.Office.Interop.Excel.XlHAlign.xlHAlignCenter;

                CurveMarksBO curveMarksBO = new CurveMarksBO
                {
                    DateRange = dateRange,
                    ValueRange = valueRange,
                    CurveShortName = curveName,
                    CurveSet = curveSet,
                    CurveStyle = curveStyle,
                    BlockName = String.Empty,
                    Periodicity = periodicity,
                    ForwardCurveType = ForwardCurveType.VOLS.ToString(),
                    CurveStatus = Global.CurveUploadStatusPending,
                    CurveStatusMsg = "Curve Not Uploaded",
                    CurveStatusDateTime = DateTime.Now,
                    MarkDate = curverMarkDate,
                    ContextID = contextID
                };
                // Add the curve marks Bo object into a list.
                curveMarksBOList.Add(curveMarksBO);

                return curveMarksBOList;
            }
            catch (Exception exception)
            {
                throw exception;
            }
        }
       
        /// <summary>
        /// CurveUploaderHeartBeat gets the curve uploader service details.
        /// </summary>
        /// <returns></returns>
        public string CurveUploaderHeartBeat()
        {
            string serviceDetails = string.Empty;
            try
            {
                CurveGateway gateway = new CurveGateway(Global.CurveWebServiceUrl);
                serviceDetails = gateway.HeartBeatFunctionality();
            }
            catch (Exception)
            {
                throw;
            }
            return serviceDetails;
        }

        /// <summary>
        /// Gets Curve Region and Context list for SignOff
        /// </summary>
        /// <returns></returns>
        public string GetCurveDataByKey(string key)
        {
            string regionContextList = string.Empty;
            try
            {
                CurveGateway gateway = new CurveGateway(Global.CurveWebServiceUrl);
                regionContextList = gateway.GetCurveData(key);
            }
            catch (Exception)
            {
                throw;
            }
            return regionContextList;
        }

        /// <summary>
        /// Gets SignOff Release history data for the given curve region and context
        /// </summary>
        /// <param name="region"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public string GetReleaseHistoryList(string region, string context)
        {
            string releaseHistory = string.Empty;
            try
            {
                CurveGateway gateway = new CurveGateway(Global.CurveWebServiceUrl);
                releaseHistory = gateway.GetSignOffData(region, context);
            }
            catch (Exception)
            {
                throw;
            }
            return releaseHistory;
        }

        public string SendSignOffData(CurveSignOffParams curveSignOffParams)
        {
            string releaseHistory = string.Empty;
            try
            {
                CurveGateway gateway = new CurveGateway(Global.CurveWebServiceUrl);
                releaseHistory = gateway.SendSignOffData(curveSignOffParams);
            }
            catch (Exception)
            {
                throw;
            }
            return releaseHistory;
        }

        public string GetProfileFolderPath(string propetyName)
        {
            string folderPath = string.Empty;
            try
            {
                CurveGateway gateway = new CurveGateway(Global.CurveWebServiceUrl);
                folderPath = gateway.GetProfileFolderPath(propetyName);
            }
            catch (Exception)
            {
                throw;
            }
            return folderPath;
        }

        public string Refresh(string context)
        {
            string result = string.Empty;
            try
            {
                CurveGateway gateway = new CurveGateway(Global.CurveWebServiceUrl);
                result = gateway.Refresh(context);
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

    }
}
