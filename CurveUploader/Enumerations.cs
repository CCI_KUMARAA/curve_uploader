﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace CurveUploader
{
    [System.SerializableAttribute()]
    public enum ForwardCurveType
    {

        /// <remarks/>
        PRICE,

        /// <remarks/>
        VOLATILITY,

        /// <remarks/>
        SPREADVOL,

        /// <remarks/>
        POLYVOL,

        VOLS
    }

    public class DownloadCurveParams
    {
        public string RequestId
        {
            get;
            set;

        }

        public string ShortName
        {
            get;
            set;
        }

        public string CurveSet
        {
            get;
            set;
        }

        public string CurveStyle
        {
            get;
            set;

        }

        public string ForwardCurveType
        {
            get;
            set;
        }

        public string BlockName
        {
            get;
            set;
        }

        public string StrikePrice
        {
            get;
            set;
        }

        public string MarkDate
        {
            get;
            set;
        }

        public string StartDate
        {
            get;
            set;
        }

        public string EndDate
        {
            get;
            set;
        }

        public string CurveData
        {
            get;
            set;
        }

        /// <summary>
        /// Get's and set's the context id property.
        /// </summary>
        public string ContextID 
        {
            get; set;
        }

        /// <summary>
        /// Get's and set's the Function Name property.
        /// </summary>
        public string FunctionName 
        {
            get;
            set; 
        }
    }

    public class Curve
    {
        public String Name
        {
            get;
            set;
        }

        public String CurveSet
        {
            get;
            set;
        }

        public String CurveStyle
        {
            get;
            set;
        }

        public String MarkDate
        {
            get;
            set;
        }

        public List<String> DateList
        {
            get;
            set;
        }

        public List<String> ValueList
        {
            get;
            set;
        }

        /// <summary>
        /// Get's and set's the context id of the curve.
        /// </summary>
        public string ContextID { get; set; }       
    }
}
