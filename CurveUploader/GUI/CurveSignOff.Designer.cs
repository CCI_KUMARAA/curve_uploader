﻿namespace CurveUploader.GUI
{
    partial class CurveSignOff
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.pnlCurve = new System.Windows.Forms.Panel();
            this.cmbContext = new System.Windows.Forms.ComboBox();
            this.cmbRegion = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlReleaseHistory = new System.Windows.Forms.Panel();
            this.pnlReason = new System.Windows.Forms.Panel();
            this.txtReason = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.lblUserId = new System.Windows.Forms.Label();
            this.lblAsOfDate = new System.Windows.Forms.Label();
            this.btnSignOff = new System.Windows.Forms.Button();
            this.btnRelease = new System.Windows.Forms.Button();
            this.rtReleaseHistory = new System.Windows.Forms.RichTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pnlCurve.SuspendLayout();
            this.pnlReleaseHistory.SuspendLayout();
            this.pnlReason.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlCurve
            // 
            this.pnlCurve.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlCurve.Controls.Add(this.cmbContext);
            this.pnlCurve.Controls.Add(this.cmbRegion);
            this.pnlCurve.Controls.Add(this.label2);
            this.pnlCurve.Controls.Add(this.label1);
            this.pnlCurve.Location = new System.Drawing.Point(12, 12);
            this.pnlCurve.Name = "pnlCurve";
            this.pnlCurve.Size = new System.Drawing.Size(349, 87);
            this.pnlCurve.TabIndex = 0;
            // 
            // cmbContext
            // 
            this.cmbContext.FormattingEnabled = true;
            this.cmbContext.Location = new System.Drawing.Point(70, 43);
            this.cmbContext.Name = "cmbContext";
            this.cmbContext.Size = new System.Drawing.Size(240, 21);
            this.cmbContext.TabIndex = 3;
            this.cmbContext.SelectedIndexChanged += new System.EventHandler(this.cmbContext_SelectedIndexChanged);
            // 
            // cmbRegion
            // 
            this.cmbRegion.FormattingEnabled = true;
            this.cmbRegion.Location = new System.Drawing.Point(70, 13);
            this.cmbRegion.Name = "cmbRegion";
            this.cmbRegion.Size = new System.Drawing.Size(240, 21);
            this.cmbRegion.TabIndex = 2;
            this.cmbRegion.SelectedIndexChanged += new System.EventHandler(this.cmbRegion_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 51);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(43, 13);
            this.label2.TabIndex = 1;
            this.label2.Text = "Context";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 21);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Region";
            // 
            // pnlReleaseHistory
            // 
            this.pnlReleaseHistory.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.pnlReleaseHistory.Controls.Add(this.pnlReason);
            this.pnlReleaseHistory.Controls.Add(this.lblUserId);
            this.pnlReleaseHistory.Controls.Add(this.lblAsOfDate);
            this.pnlReleaseHistory.Controls.Add(this.btnSignOff);
            this.pnlReleaseHistory.Controls.Add(this.btnRelease);
            this.pnlReleaseHistory.Controls.Add(this.rtReleaseHistory);
            this.pnlReleaseHistory.Controls.Add(this.label5);
            this.pnlReleaseHistory.Controls.Add(this.label4);
            this.pnlReleaseHistory.Controls.Add(this.label3);
            this.pnlReleaseHistory.Location = new System.Drawing.Point(12, 108);
            this.pnlReleaseHistory.Name = "pnlReleaseHistory";
            this.pnlReleaseHistory.Size = new System.Drawing.Size(349, 408);
            this.pnlReleaseHistory.TabIndex = 1;
            // 
            // pnlReason
            // 
            this.pnlReason.Controls.Add(this.txtReason);
            this.pnlReason.Controls.Add(this.label6);
            this.pnlReason.Location = new System.Drawing.Point(3, 287);
            this.pnlReason.Name = "pnlReason";
            this.pnlReason.Size = new System.Drawing.Size(339, 48);
            this.pnlReason.TabIndex = 9;
            // 
            // txtReason
            // 
            this.txtReason.Location = new System.Drawing.Point(67, 9);
            this.txtReason.Name = "txtReason";
            this.txtReason.Size = new System.Drawing.Size(240, 20);
            this.txtReason.TabIndex = 11;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(10, 16);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(44, 13);
            this.label6.TabIndex = 9;
            this.label6.Text = "Reason";
            // 
            // lblUserId
            // 
            this.lblUserId.AutoSize = true;
            this.lblUserId.Location = new System.Drawing.Point(120, 55);
            this.lblUserId.Name = "lblUserId";
            this.lblUserId.Size = new System.Drawing.Size(0, 13);
            this.lblUserId.TabIndex = 7;
            // 
            // lblAsOfDate
            // 
            this.lblAsOfDate.AutoSize = true;
            this.lblAsOfDate.Location = new System.Drawing.Point(120, 21);
            this.lblAsOfDate.Name = "lblAsOfDate";
            this.lblAsOfDate.Size = new System.Drawing.Size(0, 13);
            this.lblAsOfDate.TabIndex = 6;
            // 
            // btnSignOff
            // 
            this.btnSignOff.Location = new System.Drawing.Point(235, 361);
            this.btnSignOff.Name = "btnSignOff";
            this.btnSignOff.Size = new System.Drawing.Size(75, 23);
            this.btnSignOff.TabIndex = 5;
            this.btnSignOff.Text = "Sign-Off";
            this.btnSignOff.UseVisualStyleBackColor = true;
            this.btnSignOff.Click += new System.EventHandler(this.btnSignOff_Click);
            // 
            // btnRelease
            // 
            this.btnRelease.Location = new System.Drawing.Point(16, 361);
            this.btnRelease.Name = "btnRelease";
            this.btnRelease.Size = new System.Drawing.Size(75, 23);
            this.btnRelease.TabIndex = 4;
            this.btnRelease.Text = "Release";
            this.btnRelease.UseVisualStyleBackColor = true;
            this.btnRelease.Click += new System.EventHandler(this.btnRelease_Click);
            // 
            // rtReleaseHistory
            // 
            this.rtReleaseHistory.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rtReleaseHistory.Location = new System.Drawing.Point(70, 125);
            this.rtReleaseHistory.MinimumSize = new System.Drawing.Size(4, 4);
            this.rtReleaseHistory.Name = "rtReleaseHistory";
            this.rtReleaseHistory.Size = new System.Drawing.Size(240, 141);
            this.rtReleaseHistory.TabIndex = 3;
            this.rtReleaseHistory.Text = "";
            this.rtReleaseHistory.WordWrap = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(13, 92);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 13);
            this.label5.TabIndex = 2;
            this.label5.Text = "Release History";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 55);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(41, 13);
            this.label4.TabIndex = 1;
            this.label4.Text = "User Id";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 21);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(59, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "As Of Date";
            // 
            // CurveSignOff
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(375, 539);
            this.Controls.Add(this.pnlReleaseHistory);
            this.Controls.Add(this.pnlCurve);
            this.Name = "CurveSignOff";
            this.Text = "CurveSignOff";
            this.pnlCurve.ResumeLayout(false);
            this.pnlCurve.PerformLayout();
            this.pnlReleaseHistory.ResumeLayout(false);
            this.pnlReleaseHistory.PerformLayout();
            this.pnlReason.ResumeLayout(false);
            this.pnlReason.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel pnlCurve;
        private System.Windows.Forms.Panel pnlReleaseHistory;
        private System.Windows.Forms.ComboBox cmbContext;
        private System.Windows.Forms.ComboBox cmbRegion;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnSignOff;
        private System.Windows.Forms.Button btnRelease;
        private System.Windows.Forms.RichTextBox rtReleaseHistory;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label lblUserId;
        private System.Windows.Forms.Label lblAsOfDate;
        private System.Windows.Forms.Panel pnlReason;
        private System.Windows.Forms.TextBox txtReason;
        private System.Windows.Forms.Label label6;
    }
}