﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Security.Principal;
using System.Windows.Forms;

namespace CurveUploader.GUI
{
    public partial class CurveSignOff : Form
    {
        bool enableRelease = true;       
        public CurveSignOff()
        {
            InitializeComponent();
            if (cmbContext.Items.Count < 1)
            {
                LoadRegionAndContext();
            }
            SetReleasePanelVisible(false);
            SetReasonPanelVisible(false);

            if (Settings1.Default.Region != string.Empty && Settings1.Default.Context != string.Empty)
            {
                cmbRegion.Text = Settings1.Default.Region;
                cmbContext.Text = Settings1.Default.Context;
                GetData();
            }
        }

        private void LoadRegionAndContext()
        {            
            try
            {
                if (CurveMarksHelper.CurveRegionContextDictionary == null || CurveMarksHelper.CurveRegionContextDictionary.Count == 0)
                {
                    CurveUploadUtility curveUploadUtility = new CurveUploadUtility();
                    string regionContextList = curveUploadUtility.GetCurveDataByKey(Global.GET_CONTEXT_DATA);
                    LoadContextRegionDictionary(regionContextList);                    
                }
                FillRegionDropdown();
            }
            catch (Exception ex)
            {
            }           
        }

        private void SetReleasePanelVisible(bool value)
        {
            pnlReleaseHistory.Visible = value;
        }

        private void SetReasonPanelVisible(bool value)
        {
            pnlReason.Visible = value;
        }

        private void EnableReleaseButton(bool value)
        {
            btnRelease.Enabled = value;
            if (value == false)
            {
                SetReasonPanelVisible(true);
            }
        }

        private void LoadContextRegionDictionary(string regionContextList)
        {
            string[] regionContextListData = regionContextList.Split(';');
            regionContextListData = regionContextListData.Take(regionContextListData.Length - 1).ToArray();
            string key = string.Empty;
            string value = string.Empty;
            List<string> contextList = null;
            try
            {

                foreach (string item in regionContextListData)
                {
                    string[] regionContextData = item.Split(',');
                    key = regionContextData[1].Split('=')[1];
                    value = regionContextData[0].Split('=')[1];

                    if (key != string.Empty && value != string.Empty)
                    {

                        if (CurveMarksHelper.CurveRegionContextDictionary.ContainsKey(key))
                        {
                            contextList = CurveMarksHelper.CurveRegionContextDictionary[key];
                            contextList.Add(value);
                            CurveMarksHelper.CurveRegionContextDictionary[key] = contextList;
                        }
                        else
                        {
                            contextList = new List<string>();
                            contextList.Add(value);
                            CurveMarksHelper.CurveRegionContextDictionary.TryAdd(key, contextList);
                        }
                    }
                }
            }
            catch (Exception ex)
            { }
        }

        private void FillRegionDropdown()
        {
            if (CurveMarksHelper.CurveRegionContextDictionary.Count > 0)
            {
                List<string> regionList =  CurveMarksHelper.CurveRegionContextDictionary.Keys.ToList();
                regionList.Sort();
                regionList.Insert(0, "--Select--");
                cmbRegion.DataSource = regionList;
            }            
        }

        private void cmbRegion_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbRegion.SelectedIndex > 0)
            {
                string key = cmbRegion.SelectedValue.ToString();
                if (!string.IsNullOrEmpty(key))
                {
                    if (CurveMarksHelper.CurveRegionContextDictionary.ContainsKey(key))
                    {
                        List<string> contextList = CurveMarksHelper.CurveRegionContextDictionary[key];
                        contextList.Sort();
                        contextList.Insert(0, "--Select--");
                        cmbContext.DataSource = contextList;
                    }
                }
                Settings1.Default.Region =  key;               
            }
        }

        private void cmbContext_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbContext.SelectedIndex > 0)
            {
                Settings1.Default.Context = cmbContext.SelectedValue.ToString();
                GetData();
                Settings1.Default.Save();
            }          
        }

        private void GetData()
        {
            LoadReleaseHistory();
            SetReleasePanelVisible(true);
            EnableReleaseButton(enableRelease);
        }

        private void LoadReleaseHistory()
        {
            lblAsOfDate.Text = DateTime.Now.ToString();
            lblUserId.Text = WindowsIdentity.GetCurrent().Name.Split('\\')[1];
            rtReleaseHistory.Text = GetReleaseHistoryList(cmbRegion.SelectedValue.ToString(), cmbContext.SelectedValue.ToString());
        }

        private string GetReleaseHistoryList(string region, string context)
        {
            string releaseHistory = string.Empty;

            CurveUploadUtility curveUploadUtility = new CurveUploadUtility();
            releaseHistory = curveUploadUtility.GetReleaseHistoryList(region,context);
            if (releaseHistory != string.Empty)
            {
                releaseHistory =  FormatReleaseHistoryData(releaseHistory);
            }

            if (releaseHistory.ToUpper().Contains(Global.OFFICIAL_SIGNOFF))
            {
                enableRelease = false;
            }

            return releaseHistory;
        }

        private void btnRelease_Click(object sender, EventArgs e)
        {
            try
            {                
                string curveRelease = SetCurveSignOffData(Global.RELEASE_SIGNOFF);
                MessageBox.Show(curveRelease);
                this.Close();
            }
            catch (Exception ex)
            { 
            }
        }

        private void btnSignOff_Click(object sender, EventArgs e)
        {
            try
            {
                if (!enableRelease)
                {
                    if (txtReason.Text == string.Empty)
                    {
                        MessageBox.Show("Please provide reason!");
                    }
                    else 
                    {                        
                        SetCurveSignOffData(Global.OFFICIAL_SIGNOFF, txtReason.Text);
                        this.Close();
                    }
                }
                else
                {                    
                    SetCurveSignOffData(Global.OFFICIAL_SIGNOFF);
                    this.Close();
                }
            }
            catch (Exception ex)
            { 
            }

        }

        private string SetCurveSignOffData(string signOffType, string reason = "")
        {
            string releaseHistory = string.Empty;

            CurveSignOffParams curveSignOffParams = new CurveSignOffParams();

            curveSignOffParams.Region = cmbRegion.SelectedValue.ToString();
            curveSignOffParams.ContextID = cmbContext.SelectedValue.ToString();
            curveSignOffParams.MarkDate = DateTime.Now.Date.ToString("d", CultureInfo.GetCultureInfo("en-US"));
            curveSignOffParams.UserID = lblUserId.Text;
            curveSignOffParams.SignOffType = signOffType;
            curveSignOffParams.Reason = reason;

            CurveUploadUtility curveUploadUtility = new CurveUploadUtility();
            releaseHistory = curveUploadUtility.SendSignOffData(curveSignOffParams);

            return releaseHistory;
        }

        private string FormatReleaseHistoryData(string releaseHistory)
        {
            string releaseData = string.Empty;
            string[] historyArray = releaseHistory.Split(';');
            historyArray = historyArray.Take(historyArray.Length - 1).ToArray();

            foreach (string item in historyArray)
            {
                string[] historyData = item.Split(',');                
                string userID = historyData[0].Split('=')[1];
                string markDate = historyData[1].Split('=')[1];
                string signOffType = historyData[2].Split('=')[1];
                string reason = historyData[3].Split('=')[1];

                switch(signOffType)
                {
                    case Global.OFFICIAL_SIGNOFF:
                        releaseData += "Official Release by ";
                        break;
                    case Global.RELEASE_SIGNOFF:
                        releaseData += "Released by ";
                        break;
                }

                releaseData += userID + " at " + markDate + ": " + reason +  " \n";
            }

            return releaseData;
        }
    }
}
