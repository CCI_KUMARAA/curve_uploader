﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using log4net;

namespace CurveUploader.GUI
{
    public partial class FormCurveBuildStatus : Form
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(FormCurveBuildStatus));

        public FormCurveBuildStatus()
        {
            InitializeComponent();
        }

        private void LoadData()
        {

            //List<CurveBuildStatus> curveBuildStatusList = CurveMarksHelper.CurveBuildStatusList;
            //dataGridView1.DataSource = new BindingList<CurveBuildStatus>(curveBuildStatusList);

            int curveIndex = 0;

            foreach (CurveBuildStatus cbs in CurveMarksHelper.CurveBuildStatusList)
            {

                object[] obj = new object[]{Convert.ToString(++curveIndex),cbs.CCICurveName,
                                            cbs.ZEMACurveName,
                                            cbs.BuildStatus,
                                            cbs.ValidationStatus,
                                            cbs.Message
                                            };
                dataGridView1.Rows.Add(obj);

            }


            ColorGrid("buildStatus");
            ColorGrid("validationStatus");

            dataGridView1.Sort(dataGridView1.Columns["buildStatus"], ListSortDirection.Descending);
            dataGridView1.Sort(dataGridView1.Columns["validationStatus"], ListSortDirection.Descending);

            dataGridView1.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
            dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Color.WhiteSmoke;



        }
        private void ColorGrid(String column)
        {
            foreach (DataGridViewRow row in dataGridView1.Rows)
            {
                string status = row.Cells[column].Value.ToString().ToUpper();
                if (status == "ERROR" || status == "MISSING")
                {
                    row.Cells[column].Style.BackColor = Color.OrangeRed;
                    //row.Cells["CurveName"].ErrorText = "Error";
                }
                else if (status == "COMPLETE")
                {
                    row.Cells[column].Style.BackColor = Color.LimeGreen;
                    //row.Cells["CurveName"].ErrorText = "";
                }
                else
                {
                    row.Cells[column].Style.BackColor = Color.LightBlue;
                }
            }
        }

        private void dataGridView1_SortCompare(object sender, DataGridViewSortCompareEventArgs e)
        {
            if (e.Column.Index == 0)
            {
                if (Int32.Parse(e.CellValue1.ToString()) > Int32.Parse(e.CellValue2.ToString()))
                {
                    e.SortResult = 1;
                }
                else if (Int32.Parse(e.CellValue1.ToString()) < Int32.Parse(e.CellValue2.ToString()))
                {
                    e.SortResult = -1;
                }
                else
                {
                    e.SortResult = 0;
                }
                e.Handled = true;
            }
        }

        private void FormCurveBuildStatus_Load(object sender, EventArgs e)
        {
            LoadData();
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadData();
        }
    }
}

