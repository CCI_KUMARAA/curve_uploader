﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using log4net;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;
using ExcelDna.Integration;

namespace CurveUploader.GUI
{
    public partial class FormCurveListStatus : Form
    {
        /// <summary>
        /// private static logger
        /// </summary>
        private static readonly ILog _log = LogManager.GetLogger(typeof(FormCurveListStatus));

        private static FormCurveListStatus instance;
        /// <summary>
        /// Get's and set's the instance of the FormCurveListStatus.
        /// </summary>
        public static FormCurveListStatus Instance
        {
            get
            {
                if (instance == null)
                    instance = new FormCurveListStatus();
                else
                {
                    instance.WindowState = FormWindowState.Normal;
                    instance.Focus();
                    instance.BringToFront();
                }
                return instance;
            }
        }

        /// <summary>
        /// OnClosed assign null to the FormCurveListStatus instance
        /// </summary>
        /// <param name="e"></param>
        protected override void OnClosed(EventArgs e)
        {
            instance = null;
        }

        public FormCurveListStatus()
        {
            InitializeComponent();
        }

        private void FormCurveListStatus_Load(object sender, EventArgs e)
        {
            ((Excel.Application)ExcelDnaUtil.Application).Application.Calculate();
            
          //  new CurveUploadUtility().RemoveEmptyCellCurves(CurveMarksHelper.CurveUploadDictionary);
            
            loadData();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            dataGridView1.Rows.Clear();

            loadData();
        }

        public void loadData()
        {
            try
            {

                int curveIndex = 0;

                foreach (KeyValuePair<string, CurveMarksBO> kvp in CurveMarksHelper.CurveUploadDictionary)
                {
                    
                    object[] obj = new object[]{Convert.ToString(++curveIndex),kvp.Value.CurveShortName, 
                                            kvp.Value.CurveSet, 
                                            kvp.Value.CurveStyle, 
                                            kvp.Value.CurveStatus,
                                            kvp.Value.CurveStatusMsg,
                                            kvp.Value.CurveStatusDateTime.ToString("MM/dd/yyyy hh:mm tt")
                                            };
                    dataGridView1.Rows.Add(obj);

                }

                foreach (DataGridViewRow row in dataGridView1.Rows)
                {
                    string status = row.Cells["Status"].Value.ToString();
                    if (status == "ERROR")
                    {
                        row.Cells["Status"].Style.BackColor = Color.OrangeRed;
                        row.Cells["CurveName"].ErrorText = "Error";
                    }
                    else if(status == "SUCCESS")
                    {
                        row.Cells["Status"].Style.BackColor = Color.LimeGreen;
                        row.Cells["CurveName"].ErrorText = "";
                    }
                    else
                    {
                        row.Cells["Status"].Style.BackColor = Color.LightBlue;
                    }
                }
              
                dataGridView1.Sort(dataGridView1.Columns["Status"], ListSortDirection.Ascending);
                dataGridView1.AutoResizeColumns(DataGridViewAutoSizeColumnsMode.AllCells);
                

                dataGridView1.AlternatingRowsDefaultCellStyle.BackColor = Color.WhiteSmoke;
                // If the datagrid does not contain any row, then disable the copy to clipboard button.
                GetCopyToClipboardButtonState();
            }
            catch (Exception e)
            {
                _log.Error("FormCurveListStats.loadData(): ", e);
            }
        }

        /// <summary>
        /// GetCopyToClipboardButtonState enables/ disables the button depending upon the data grid row count.
        /// </summary>
        private void GetCopyToClipboardButtonState()
        {
            if (dataGridView1.Rows.Count == 0)
                btnCopyToClipboard.Enabled = false;
            else
                btnCopyToClipboard.Enabled = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
            instance = null;
        }

        private void dataGridView1_SortCompare(object sender, DataGridViewSortCompareEventArgs e)
        {
            if (e.Column.Index == 0)
            {
                if (Int32.Parse(e.CellValue1.ToString()) > Int32.Parse(e.CellValue2.ToString()))
                {
                    e.SortResult = 1;
                }
                else if (Int32.Parse(e.CellValue1.ToString()) < Int32.Parse(e.CellValue2.ToString()))
                {
                    e.SortResult = -1;
                }
                else
                {
                    e.SortResult = 0;
                }
                e.Handled = true;
            }
        }

        /// <summary>
        /// Copies the selected rows to clipboard.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCopyToClipboard_Click(object sender, EventArgs e)
        {
            this.dataGridView1.SelectAll();
            if (this.dataGridView1.GetCellCount(DataGridViewElementStates.Selected) > 0)
            {
                try
                {
                    // Add the selection to the clipboard.
                    Clipboard.SetDataObject(
                        this.dataGridView1.GetClipboardContent());
                }
                catch (System.Runtime.InteropServices.ExternalException)
                {

                }
                finally
                {
                    this.dataGridView1.ClearSelection();
                }
            }
        }

    }
}
