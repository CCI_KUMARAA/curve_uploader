﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;


namespace CurveUploader
{
    public partial class FormDownloadSingleCurve : Form
    {
        public FormDownloadSingleCurve()
        {
            InitializeComponent();
        }

        private void FormDownloadSingleCurve_Load(object sender, EventArgs e)
        {
            //Globals.ThisAddIn.Application.Calculate();

            //Globals.ThisAddIn.RemoveEmptyCellCurves(CurveMarksHelper.CurveDownloadDictionary);

            foreach (KeyValuePair<string, CurveMarksBO> kvp in CurveMarksHelper.CurveDownloadDictionary)
            {
                string[] tokens = kvp.Key.Split('@');
                string workbook = tokens[0];
                string worksheetIndex = tokens[1];
                string cell = tokens[2];

                comboBox1.Items.Add(String.Format("{0}  -  Cell: {1}  -  ${2}", kvp.Value.CurveShortName, cell, kvp.Key));
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem != null)
            {
                //Globals.ThisAddIn.DownloadCurveList(comboBox1.SelectedItem.ToString());
                //Globals.ThisAddIn.resetOffsetCache();
                CurveUploadUtility curveUploadUtility = new CurveUploadUtility();
                curveUploadUtility.DownloadCurveList(comboBox1.SelectedItem.ToString());
                this.Close();
            }
            else
                MessageBox.Show("Please select a Forward Curve");
        }


        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }        
    }
}
