﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace CurveUploader.GUI
{
    public partial class FormEnvironment : Form
    {
        public FormEnvironment()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (radioProduction.Checked)
                Global.CurveWebServiceUrl = Global.ProductionCurveWebServiceUrl;
            else if (radioQA.Checked)
                Global.CurveWebServiceUrl = Global.QACurveWebServiceUrl;
            else if (radioDevelopment.Checked)
                Global.CurveWebServiceUrl = Global.DevelopmentCurveWebServiceUrl;
            else if (radioButtonLocal.Checked)
                Global.CurveWebServiceUrl = Global.LocalCurveWebServiceUrl;
         

            Close();
        }

        private void FormEnvironment_Load(object sender, EventArgs e)
        {
            if (Global.CurveWebServiceUrl == Global.ProductionCurveWebServiceUrl)
                radioProduction.Checked = true;
            else if (Global.CurveWebServiceUrl == Global.QACurveWebServiceUrl)
                radioQA.Checked = true;
            else if (Global.CurveWebServiceUrl == Global.DevelopmentCurveWebServiceUrl)
                radioDevelopment.Checked = true;
            else if (Global.CurveWebServiceUrl == Global.LocalCurveWebServiceUrl)
                radioButtonLocal.Checked = true;
           

            if (Global.ConnectionInitialized)
            {
                MessageBox.Show("Please restart Excel to change Environment", Global.MessageBoxTitle);

                radioProduction.Enabled = false;
                radioQA.Enabled = false;
                radioDevelopment.Enabled = false;
                radioButtonLocal.Enabled = false;
               
            }
        }

        private void radioQA_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void radioProduction_CheckedChanged(object sender, EventArgs e)
        {

        }

      
    }
}
