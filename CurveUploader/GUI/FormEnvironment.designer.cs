﻿namespace CurveUploader.GUI
{
    partial class FormEnvironment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioDevelopment = new System.Windows.Forms.RadioButton();
            this.radioQA = new System.Windows.Forms.RadioButton();
            this.radioProduction = new System.Windows.Forms.RadioButton();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.radioButtonLocal = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(149, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "Please Select an Environment";
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButtonLocal);
            this.groupBox1.Controls.Add(this.radioDevelopment);
            this.groupBox1.Controls.Add(this.radioQA);
            this.groupBox1.Controls.Add(this.radioProduction);
            this.groupBox1.Location = new System.Drawing.Point(16, 36);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(212, 77);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Environment";
            // 
            // radioDevelopment
            // 
            this.radioDevelopment.AutoSize = true;
            this.radioDevelopment.Location = new System.Drawing.Point(17, 43);
            this.radioDevelopment.Name = "radioDevelopment";
            this.radioDevelopment.Size = new System.Drawing.Size(88, 17);
            this.radioDevelopment.TabIndex = 2;
            this.radioDevelopment.Text = "Development";
            this.radioDevelopment.UseVisualStyleBackColor = true;
            // 
            // radioQA
            // 
            this.radioQA.AutoSize = true;
            this.radioQA.Location = new System.Drawing.Point(117, 20);
            this.radioQA.Name = "radioQA";
            this.radioQA.Size = new System.Drawing.Size(70, 17);
            this.radioQA.TabIndex = 1;
            this.radioQA.Text = "QA - Test";
            this.radioQA.UseVisualStyleBackColor = true;
            // 
            // radioProduction
            // 
            this.radioProduction.AutoSize = true;
            this.radioProduction.Location = new System.Drawing.Point(17, 20);
            this.radioProduction.Name = "radioProduction";
            this.radioProduction.Size = new System.Drawing.Size(76, 17);
            this.radioProduction.TabIndex = 0;
            this.radioProduction.Text = "Production";
            this.radioProduction.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(39, 119);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(75, 23);
            this.button1.TabIndex = 2;
            this.button1.Text = "Save";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(123, 120);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(75, 23);
            this.button2.TabIndex = 3;
            this.button2.Text = "Cancel";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // radioButtonLocal
            // 
            this.radioButtonLocal.AutoSize = true;
            this.radioButtonLocal.Location = new System.Drawing.Point(117, 43);
            this.radioButtonLocal.Name = "radioButtonLocal";
            this.radioButtonLocal.Size = new System.Drawing.Size(51, 17);
            this.radioButtonLocal.TabIndex = 3;
            this.radioButtonLocal.Text = "Local";
            this.radioButtonLocal.UseVisualStyleBackColor = true;
            // 
            // FormEnvironment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(231, 150);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow;
            this.Name = "FormEnvironment";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = " Environment";
            this.Load += new System.EventHandler(this.FormEnvironment_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioQA;
        private System.Windows.Forms.RadioButton radioProduction;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.RadioButton radioDevelopment;
        private System.Windows.Forms.RadioButton radioButtonLocal;
    }
}