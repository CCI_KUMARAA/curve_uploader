﻿using System;
using System.Windows.Forms;

namespace CurveUploader.GUI
{
    public partial class FormForwardCurveDialog : Form
    {
        public FormForwardCurveDialog()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
                Global.MarkDate = null;
            else
            {
                Global.MarkDate = dateTimePicker1.Value.ToString("o");
                CurveMarksHelper.UpdateMarkDate();
            }

            Close();
        }


        private void FormMarkDate_Load(object sender, EventArgs e)
        {
            dateTimePicker1.MaxDate = DateTime.Now;

            try
            {
                if (Global.MarkDate != null)
                {
                    checkBox1.Checked = false;
                    dateTimePicker1.Value = DateTime.Parse(Global.MarkDate);
                }
                else
                {
                    checkBox1.Checked = true;
                    dateTimePicker1.Value = DateTime.Now;
                }
            }
            catch (Exception)
            {
                dateTimePicker1.Value = DateTime.Now;
            }
        }

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBox1.Checked)
            {
                dateTimePicker1.Enabled = false;
                dateTimePicker1.Value = DateTime.Now;
            }
            else
                dateTimePicker1.Enabled = true;
        }
    }
}
