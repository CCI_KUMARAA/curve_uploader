﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;


namespace CurveUploader.GUI
{
    public partial class FormUploadSingleCurve : Form
    {
        public FormUploadSingleCurve()
        {
            InitializeComponent();
        }

        private void FormUploadSingleCurve_Load(object sender, EventArgs e)
        {
            //Globals.ThisAddIn.Application.Calculate();

            //Globals.ThisAddIn.RemoveEmptyCellCurves(CurveMarksHelper.CurveUploadDictionary);

            foreach (KeyValuePair<string, CurveMarksBO> kvp in CurveMarksHelper.CurveUploadDictionary)
            {
                string[] tokens = kvp.Key.Split('@');
                string workbook = tokens[0];
                string worksheetIndex = tokens[1];
                string cell = tokens[2];

                comboBox1.Items.Add(String.Format("{0}  -  Cell: {1}  -  ${2}", kvp.Value.CurveShortName, cell, kvp.Key));
            }          
            
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox1.SelectedItem != null)
            {
                CurveUploadUtility curveUploadUtility = new CurveUploadUtility();
                curveUploadUtility.UploadCurveList(comboBox1.SelectedItem.ToString(), false);
                this.Close();
            }
            else
                MessageBox.Show("Please select a Forward Curve", Global.MessageBoxTitle);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
