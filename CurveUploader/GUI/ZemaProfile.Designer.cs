﻿namespace CurveUploader.GUI
{
    partial class ZemaProfile
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ZemaProfile));
            this.panel1 = new System.Windows.Forms.Panel();
            this.lvNY = new System.Windows.Forms.ListView();
            this.lvSG = new System.Windows.Forms.ListView();
            this.label3 = new System.Windows.Forms.Label();
            this.lvLN = new System.Windows.Forms.ListView();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ilstCurveImage = new System.Windows.Forms.ImageList(this.components);
            this.cmsCurveMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.tsmUpload = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmOpen = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmRefresh = new System.Windows.Forms.ToolStripMenuItem();
            this.tsmSignOff = new System.Windows.Forms.ToolStripMenuItem();
            this.panel1.SuspendLayout();
            this.cmsCurveMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.lvNY);
            this.panel1.Controls.Add(this.lvSG);
            this.panel1.Controls.Add(this.label3);
            this.panel1.Controls.Add(this.lvLN);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(12, 7);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(838, 534);
            this.panel1.TabIndex = 0;
            // 
            // lvNY
            // 
            this.lvNY.Location = new System.Drawing.Point(37, 78);
            this.lvNY.Margin = new System.Windows.Forms.Padding(10);
            this.lvNY.MultiSelect = false;
            this.lvNY.Name = "lvNY";
            this.lvNY.Size = new System.Drawing.Size(222, 424);
            this.lvNY.TabIndex = 6;
            this.lvNY.UseCompatibleStateImageBehavior = false;
            this.lvNY.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lvNY_MouseClick);
            // 
            // lvSG
            // 
            this.lvSG.Location = new System.Drawing.Point(584, 78);
            this.lvSG.MultiSelect = false;
            this.lvSG.Name = "lvSG";
            this.lvSG.Size = new System.Drawing.Size(222, 424);
            this.lvSG.TabIndex = 5;
            this.lvSG.UseCompatibleStateImageBehavior = false;
            this.lvSG.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lvSG_MouseClick);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(584, 38);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(51, 13);
            this.label3.TabIndex = 4;
            this.label3.Text = "SG_EOD";
            // 
            // lvLN
            // 
            this.lvLN.Location = new System.Drawing.Point(309, 78);
            this.lvLN.Margin = new System.Windows.Forms.Padding(10);
            this.lvLN.MultiSelect = false;
            this.lvLN.Name = "lvLN";
            this.lvLN.Size = new System.Drawing.Size(222, 424);
            this.lvLN.TabIndex = 3;
            this.lvLN.UseCompatibleStateImageBehavior = false;
            this.lvLN.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lvLN_MouseClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(309, 38);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(50, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "LN_EOD";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(34, 38);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(51, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "NY_EOD";
            // 
            // ilstCurveImage
            // 
            this.ilstCurveImage.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("ilstCurveImage.ImageStream")));
            this.ilstCurveImage.TransparentColor = System.Drawing.Color.Transparent;
            this.ilstCurveImage.Images.SetKeyName(0, "Kuznets_curve.png");
            // 
            // cmsCurveMenu
            // 
            this.cmsCurveMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.tsmUpload,
            this.tsmOpen,
            this.tsmRefresh,
            this.tsmSignOff});
            this.cmsCurveMenu.Name = "cmsCurveMenu";
            this.cmsCurveMenu.Size = new System.Drawing.Size(153, 114);
            // 
            // tsmUpload
            // 
            this.tsmUpload.Name = "tsmUpload";
            this.tsmUpload.Size = new System.Drawing.Size(152, 22);
            this.tsmUpload.Text = "Upload";
            this.tsmUpload.ToolTipText = "Upload";
            this.tsmUpload.Click += new System.EventHandler(this.tsmUpload_Click);
            // 
            // tsmOpen
            // 
            this.tsmOpen.Name = "tsmOpen";
            this.tsmOpen.Size = new System.Drawing.Size(152, 22);
            this.tsmOpen.Text = "Open";
            this.tsmOpen.ToolTipText = "Open";
            this.tsmOpen.Click += new System.EventHandler(this.tsmOpen_Click);
            // 
            // tsmRefresh
            // 
            this.tsmRefresh.Name = "tsmRefresh";
            this.tsmRefresh.Size = new System.Drawing.Size(152, 22);
            this.tsmRefresh.Text = "Refresh";
            this.tsmRefresh.ToolTipText = "Refresh";
            this.tsmRefresh.Click += new System.EventHandler(this.tsmRefresh_Click);
            // 
            // tsmSignOff
            // 
            this.tsmSignOff.Name = "tsmSignOff";
            this.tsmSignOff.Size = new System.Drawing.Size(152, 22);
            this.tsmSignOff.Text = "SignOff";
            this.tsmSignOff.ToolTipText = "SignOff";
            this.tsmSignOff.Click += new System.EventHandler(this.tsmSignOff_Click);
            // 
            // ZemaProfile
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(859, 554);
            this.Controls.Add(this.panel1);
            this.Name = "ZemaProfile";
            this.Text = "ZemaProfile";
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.cmsCurveMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.ListView lvSG;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ListView lvLN;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ImageList ilstCurveImage;
        private System.Windows.Forms.ListView lvNY;
        private System.Windows.Forms.ContextMenuStrip cmsCurveMenu;
        private System.Windows.Forms.ToolStripMenuItem tsmOpen;
        private System.Windows.Forms.ToolStripMenuItem tsmUpload;
        private System.Windows.Forms.ToolStripMenuItem tsmRefresh;
        private System.Windows.Forms.ToolStripMenuItem tsmSignOff;



    }
}