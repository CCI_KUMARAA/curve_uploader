﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using System.Drawing;

namespace CurveUploader.GUI
{
    public partial class ZemaProfile : Form
    {
        Dictionary<string, List<string>> curveNameByRegion = new Dictionary<string, List<string>>();
        public ZemaProfile()
        {
            InitializeComponent();
            ExtractZemaProfiles();
        }

        private void ExtractZemaProfiles()
        {           
           try
           {
               CurveUploadUtility curveUploadUtility = new CurveUploadUtility();
               string curveName = curveUploadUtility.GetCurveDataByKey(Global.GET_CONTEXT_BY_REGION);
               LoadCurveDictionary(curveName);

               AssignListToListView(lvLN, curveNameByRegion[Global.CURVE_REGION_LONDON]);
               AssignListToListView(lvNY, curveNameByRegion[Global.CURVE_REGION_NEWYORK]);
               AssignListToListView(lvSG, curveNameByRegion[Global.CURVE_REGION_SINGAPORE]);

               ClearListViewSelection();               
           }
           catch (Exception ex)
           {
           }           
        }

        private void ClearListViewSelection()
        {
            lvLN.SelectedItems.Clear();
            lvNY.SelectedItems.Clear();
            lvSG.SelectedItems.Clear();
        }

        private void LoadCurveDictionary(string allCurveName)
        {            
            List<string> curveNameList = new List<string>();

            string[] curveList = allCurveName.Split(';');
            curveList = curveList.Take(curveList.Length - 1).ToArray();
            string key = string.Empty;
            string value = string.Empty;
            foreach (string curveName in curveList)
            {               
                key = curveName.Split(',')[1].Split('=')[1];
                value = curveName.Split(',')[0].Split('=')[1];

                if (key != string.Empty && value != string.Empty)
                {

                    if (curveNameByRegion.ContainsKey(key))
                    {
                        curveNameList = curveNameByRegion[key];
                        curveNameList.Add(value);
                        curveNameByRegion[key] = curveNameList;
                    }
                    else
                    {
                        curveNameList = new List<string>();
                        curveNameList.Add(value);
                        curveNameByRegion.Add(key, curveNameList);
                    }
                }
            }           
        }         

        private void AssignListToListView(ListView lstView, List<string> curveList)
        {
            lstView.View = View.Details;
            int imageHeight = 25;
            ImageList imgList1 = new ImageList();
            imgList1.ImageSize = new System.Drawing.Size(1, imageHeight);         
            //imgList1.Images.Add(Image.FromFile("..//Images//curve.png"));
            lstView.SmallImageList = imgList1;

            lstView.Columns.Add(string.Empty, 400, HorizontalAlignment.Left);

            int i = 0;

            foreach (string curve in curveList)
            {
                lstView.Items.Add(curve, i);
                i++;
            }

        }

        private void OpenSheet(string curveName, string fileType)
        {
            if (fileType.Equals(Global.CURVE_UPLOAD_FOLDER_PATH))
            {
                curveName = "\\CU-" + curveName + ".xlsm";
            }
            else
            {
                curveName = "\\" + curveName + ".xlsm";
            }
            string fileName = GetFilePath(fileType) + curveName; 
            if (File.Exists(fileName))
            {              
                ProcessStartInfo pi = new ProcessStartInfo();
                pi.FileName = "EXCEL.EXE";
                pi.Arguments = fileName;
                Process.Start(pi);
            }
            else
            { 
                MessageBox.Show("File " + fileName + " does not exist!");
            }           
        }

        private string GetFilePath(string fileType)
        {
            CurveUploadUtility curveUploadUtility = new CurveUploadUtility();
            return curveUploadUtility.GetProfileFolderPath(fileType);
        }

        private void lvNY_MouseClick(object sender, MouseEventArgs e)
        {
            ListView lstVw = sender as ListView;

            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                ListViewItem item = lstVw.GetItemAt(e.X, e.Y);
                if (item != null)
                {
                    item.Selected = true;                  
                    cmsCurveMenu.Show(lstVw, e.Location);
                }
            }
        }      

        private void lvLN_MouseClick(object sender, MouseEventArgs e)
        {
            ListView lstVw = sender as ListView;

            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                ListViewItem item = lstVw.GetItemAt(e.X, e.Y);
                if (item != null)
                {
                    item.Selected = true;
                    cmsCurveMenu.Show(lstVw, e.Location);
                }
            }
        }       

        private void lvSG_MouseClick(object sender, MouseEventArgs e)
        {
            ListView lstVw = sender as ListView;

            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                ListViewItem item = lstVw.GetItemAt(e.X, e.Y);
                if (item != null)
                {
                    item.Selected = true;                  
                    cmsCurveMenu.Show(lstVw, e.Location);
                }
            }
        }

        private void tsmUpload_Click(object sender, EventArgs e)
        {
            string[] curveDetail = GetContextName().Split(';');
            OpenSheet(curveDetail[1], Global.CURVE_UPLOAD_FOLDER_PATH);
        }

        private void tsmOpen_Click(object sender, EventArgs e)
        {
            string[] curveDetail = GetContextName().Split(';');
            OpenSheet(curveDetail[1], Global.SIGNOFF_FOLDER_PATH);
        }

        private void tsmRefresh_Click(object sender, EventArgs e)
        {
            string[] curveDetail = GetContextName().Split(';');
            CurveUploadUtility curveUploadUtility = new CurveUploadUtility();
            string result = curveUploadUtility.Refresh(curveDetail[1]);

            MessageBox.Show(result);
        }

        private void tsmSignOff_Click(object sender, EventArgs e)
        {
            string[] curveDetail = GetContextName().Split(';');
            Settings1.Default.Region = curveDetail[0];
            Settings1.Default.Context = curveDetail[1];

            CurveSignOff curveSignOff = new CurveSignOff();
            curveSignOff.Show();
        }

        private string GetContextName()
        {
            string curveDetail = string.Empty;

            if (lvLN.SelectedItems.Count > 0)
            {
                curveDetail = Global.CURVE_REGION_LONDON + ";" + lvLN.SelectedItems[0].Text;                
            }
            else if (lvNY.SelectedItems.Count > 0)
            {
                curveDetail = Global.CURVE_REGION_NEWYORK + ";" + lvNY.SelectedItems[0].Text;
            }
            else if (lvSG.SelectedItems.Count > 0)
            {
                curveDetail = Global.CURVE_REGION_SINGAPORE + ";" + lvSG.SelectedItems[0].Text;
            }

            ClearListViewSelection();

            return curveDetail;
        }
    }
}
