﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using log4net;


namespace CurveUploader
{
    public class Global
    {

        private static readonly ILog _log = LogManager.GetLogger(typeof(Global));   

        public static string ProductionCurveWebServiceUrl = " http://stmfop01:7301/CurveService";
        public static string QACurveWebServiceUrl = " http://stmfop01:9999/CurveService";
        public static string DevelopmentCurveWebServiceUrl = " http://stmfod01:7301/CurveService";
        public static string LocalCurveWebServiceUrl = " http://localhost:7301/CurveService";

        //public static string CurveWebServiceUrl = "http://stmfoq01:7305/CurveService";
        public static string CurveWebServiceUrl = "http://wltdevel02:7301/CurveService";    

        /// <summary>
        /// StagingWebServiceUrl web service URL for staging environment.
        /// </summary>
        public static string StagingWebServiceUrl = " http://wltdevel02:7303/CurveService";

        public const string RegisteredCurveMsgReady = "Registered!";
        public const string RegisteredCurveDownloadMsg = "DownloadReady!";
        public const string RegisteredCurveMsgInvalid = "Invalid Ranges!";
        public const string CurveUploadStatusSuccess = "Successful";
        public const string CurveUploadStatusFailure = "Failure";
        public const string CurveUploadStatusPending = "Pending";

        // Date used for uploading curves
        public static string MarkDate;

        public static string ClientID;

        public static string MessageBoxTitle = "Curve Uploader";

        public static string CurveUploadGroup = "rights-Energy-Excel-CurveUpload";

        public static string CurveUploadAdmin = "GBL-CurveUpload-Admin";

        public static string ServiceHostException = string.Empty;

        public static bool IsUserCurveUploadAdmin = false;

        public static bool ConnectionInitialized { get; set; }

        public static void DisplayMsg(string msg, bool displayMessageBox = true)
        {
            if (displayMessageBox)
                MessageBox.Show(msg, Global.MessageBoxTitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
            else
                _log.Debug(msg);
        }

        public static bool DisplayMsgYesNo(string msg, bool displayMessageBox = true)
        {
            if (displayMessageBox)
                return MessageBox.Show(msg, Global.MessageBoxTitle, MessageBoxButtons.YesNo) == DialogResult.Yes;
            else
                _log.Debug(msg);

            return true;
        }

        /// <summary>
        /// Constant for CCI Context
        /// </summary>
        public const string CCI_CONTEXT = "CCI";

        /// <summary>
        /// Constant for VOLS Forward Curve Type
        /// </summary>
        public const string VOLS = "VOLS";

        /// <summary>
        /// Constant for representing context key passed as parameter to getCurveData
        /// </summary>
        public const string GET_CONTEXT_DATA = "GET_CONTEXT_DATA";

        /// <summary>
        /// Constant for reresenting Sign off folder path key
        /// </summary>
        public const string SIGNOFF_FOLDER_PATH = "SIGNOFF_FOLDER_PATH";

        /// <summary>
        /// Constant for Signoff Type
        /// </summary>
        public const string RELEASE_SIGNOFF = "RELEASE";

        /// <summary>
        /// Constant for Signoff Type
        /// </summary>
        public const string OFFICIAL_SIGNOFF = "OFFICIAL";

        /// <summary>
        /// Constant for representing context key passed as parameter to getCurveData
        /// </summary>
        public const string GET_CONTEXT_BY_REGION = "GET_CONTEXT_BY_REGION";

        public const string CURVE_REGION_LONDON = "LN_EOD";
        public const string CURVE_REGION_NEWYORK = "NY_EOD";
        public const string CURVE_REGION_SINGAPORE = "SG_EOD";
        public const string CURVE_UPLOAD_FOLDER_PATH = "CURVE_UPLOAD_FOLDER_PATH";

    }
}
