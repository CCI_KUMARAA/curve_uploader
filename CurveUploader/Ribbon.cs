﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.InteropServices;
using ExcelDna.Integration.CustomUI;
using ExcelDna.Integration;

using System.Xml;
using System.IO;
using System.Reflection;
using System.Diagnostics;
using System.Collections.Concurrent;

using log4net;

using System.Security.Principal;
using Excel = Microsoft.Office.Interop.Excel;
using CurveUploader.GUI;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;

namespace CurveUploader
{
    [ComVisible(true)]
    public class Ribbon : ExcelDna.Integration.CustomUI.ExcelRibbon, IExcelAddIn
    {        
        private static readonly ILog _log = LogManager.GetLogger(typeof(Ribbon));

        public override string GetCustomUI(string uiName)
        {
            XmlDocument doc = new XmlDocument();
           
            Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("CurveUploader.Ribbon.xml");
            doc.Load(stream);


            if (!CurveMarksHelper.IsCurveUploadUser(WindowsIdentity.GetCurrent().Name.Split('\\')[1]))
                return null;
            else
                return doc.InnerXml;
        }

        public override void OnDisconnection(ExcelDna.Integration.Extensibility.ext_DisconnectMode RemoveMode, ref Array custom)
        {
            //base.OnDisconnection(RemoveMode, ref custom);
        }

        public override void OnBeginShutdown(ref Array custom)
        {
            ((Excel.Application)ExcelDnaUtil.Application).Quit();
            CurveMarksHelper.ReleaseComObject((Excel.Application)ExcelDnaUtil.Application);

            GC.WaitForPendingFinalizers();
            GC.Collect();
            GC.WaitForPendingFinalizers();
            GC.Collect();


        }

        public void AddSuffix_Click(IRibbonControl control)
        {
            Excel.Application app = ((Excel.Application)ExcelDnaUtil.Application);
            app.DisplayAlerts = false;

            foreach (Worksheet sheet in app.ActiveWorkbook.Sheets)
            {
                bool status = false;

                status = sheet.Cells.Replace("RegisterCurve(", "RegisterCurveX(", Excel.XlLookAt.xlPart,
                                    Excel.XlSearchOrder.xlByRows, false, Type.Missing, Type.Missing);
                status = sheet.Cells.Replace("RegisterCurveDetail(", "RegisterCurveDetailX(", Excel.XlLookAt.xlPart,
                    Excel.XlSearchOrder.xlByRows, false, Type.Missing, Type.Missing);
                status = sheet.Cells.Replace("RegisterVolatilityCurve(", "RegisterVolatilityCurveX(", Excel.XlLookAt.xlPart,
                    Excel.XlSearchOrder.xlByRows, false, Type.Missing, Type.Missing);
                status = sheet.Cells.Replace("RegisterVolatilityCurveDetail(", "RegisterVolatilityCurveDetailX(", Excel.XlLookAt.xlPart,
                    Excel.XlSearchOrder.xlByRows, false, Type.Missing, Type.Missing);
                status = sheet.Cells.Replace("RegisterPipelineCurve(", "RegisterPipelineCurveX(", Excel.XlLookAt.xlPart,
                    Excel.XlSearchOrder.xlByRows, false, Type.Missing, Type.Missing);


            }
            app.DisplayAlerts = true;
            MessageBox.Show("Changed formulas to Curve Uploader format.", Global.MessageBoxTitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public void RemoveSuffix_Click(IRibbonControl control)
        {
            Excel.Application app = ((Excel.Application)ExcelDnaUtil.Application);
            app.DisplayAlerts = false;

            foreach (Worksheet sheet in app.ActiveWorkbook.Sheets)
            {
                bool status = false;

                status = sheet.Cells.Replace("RegisterCurveX(", "RegisterCurve(", Excel.XlLookAt.xlPart,
                                    Excel.XlSearchOrder.xlByRows, false, Type.Missing, Type.Missing);
                status = sheet.Cells.Replace("RegisterCurveDetailX(", "RegisterCurveDetail(", Excel.XlLookAt.xlPart,
                    Excel.XlSearchOrder.xlByRows, false, Type.Missing, Type.Missing);
                status = sheet.Cells.Replace("RegisterVolatilityCurveX(", "RegisterVolatilityCurve(", Excel.XlLookAt.xlPart,
                    Excel.XlSearchOrder.xlByRows, false, Type.Missing, Type.Missing);
                status = sheet.Cells.Replace("RegisterVolatilityCurveDetailX(", "RegisterVolatilityCurveDetail(", Excel.XlLookAt.xlPart,
                    Excel.XlSearchOrder.xlByRows, false, Type.Missing, Type.Missing);
                status = sheet.Cells.Replace("RegisterPipelineCurveX(", "RegisterPipelineCurve(", Excel.XlLookAt.xlPart,
                    Excel.XlSearchOrder.xlByRows, false, Type.Missing, Type.Missing);


            }
            app.DisplayAlerts = true;
            MessageBox.Show("Changed formulas to Castleton Uploader format.", Global.MessageBoxTitle, MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        public void AboutButton_Click(IRibbonControl control)
        {
            AboutBox aboutBox = new AboutBox();
            aboutBox.ShowDialog();
        }

        public void ShowCurveList_Click(IRibbonControl control)
        {
            //FormCurveListStatus curveListStatus = new FormCurveListStatus();
            //curveListStatus.ShowDialog();
            FormCurveListStatus.Instance.Show();
        }

        public void UploadSingleButton_Click(IRibbonControl control)
        {
            using (FormUploadSingleCurve form = new FormUploadSingleCurve())
            {
                form.ShowDialog();
            }
        }

        public void UploadButton_Click(IRibbonControl control)
        {


            CurveUploadUtility curveUploadUtility = new CurveUploadUtility();
            bool uploadSuccess = curveUploadUtility.UploadCurveList(null, false);

            if (uploadSuccess)
            {
                CurveBuildUtility curveBuildUtility = new CurveBuildUtility();
                List<CurveBuildStatus> curveBuildStatusList = curveBuildUtility.BuildAllCurves();
            }

        }

        public void ShowCurveBuildStatus_Click(IRibbonControl control)
        {
            FormCurveBuildStatus cbs = new FormCurveBuildStatus();
            cbs.ShowDialog();
        }

        //ShowCurveBuildStatus_Click

        public void MarkDateSet_Click(IRibbonControl control)
        {
            using (FormForwardCurveDialog form = new FormForwardCurveDialog())
            {
                form.ShowDialog();
            }
        }

        public void ClearCurveList_Click(IRibbonControl control)
        {
            DialogResult result = MessageBox.Show("Reset all registered curves?", Global.MessageBoxTitle, MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (result == DialogResult.Yes)
            {
                CurveMarksHelper.CurveUploadDictionary.Clear();
            }
        }

        public void DownloadSingleButton_Click(IRibbonControl control)
        {
            using (FormDownloadSingleCurve form = new FormDownloadSingleCurve())
            {
                form.ShowDialog();
            }
        }

        public void DownloadButton_Click(IRibbonControl control)
        {

            CurveUploadUtility curveUploadUtility = new CurveUploadUtility();
            curveUploadUtility.DownloadCurveList(null);

        }

        public void ResetCurveStatus_Click(IRibbonControl control)
        {
            ((Excel.Application)ExcelDnaUtil.Application).Calculate();
            CurveUploadUtility utility = new CurveUploadUtility();

            utility.ResetCurveListStatus();

        }

        public void SignOffButton_Click(IRibbonControl control)
        {
            CurveSignOff curveSignOff = new CurveSignOff();
            curveSignOff.ShowDialog();
        }

        public void Open_Click(IRibbonControl control)
        {
            ZemaProfile zemaProfile = new ZemaProfile();
            zemaProfile.ShowDialog();
        }

        public void AutoClose()
        {
        }

        public void AutoOpen()
        {
            Global.IsUserCurveUploadAdmin = CurveMarksHelper.IsCurveUploadAdmin((WindowsIdentity.GetCurrent().Name.Split('\\')[1]));

            ((Excel.Application)ExcelDnaUtil.Application).WorkbookBeforeClose += new AppEvents_WorkbookBeforeCloseEventHandler(Application_WorkbookBeforeClose);

        }

        void Application_WorkbookBeforeClose(Excel.Workbook Wb, ref bool Cancel)
        {
            if (CurveMarksHelper.CurveUploadDictionary != null)
                CurveMarksHelper.CurveUploadDictionary.Clear();

            if (CurveMarksHelper.CurveDownloadDictionary != null)
                CurveMarksHelper.CurveDownloadDictionary.Clear();

        }

    }
}
