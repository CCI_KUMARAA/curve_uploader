﻿using CurveUploader.GUI;
using ExcelDna.Integration;
using log4net;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;
using Excel = Microsoft.Office.Interop.Excel;

namespace CurveUploader
{
    public class UDF
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(UDF));



        [ExcelFunction(IsMacroType = true, Category = "Curve Uploader")]
        public static string UploadAllCurves()
        {
            CurveUploadUtility curveUploadUtility = new CurveUploadUtility();
            bool uploadStatus = curveUploadUtility.UploadCurveList(null, false, false);
            if (uploadStatus)
            {
                CurveBuildUtility curveBuildUtility = new CurveBuildUtility();
                List<CurveBuildStatus> curveBuildStatusList = curveBuildUtility.BuildAllCurves(false);
            }

            return string.Empty;

        }

        [ExcelFunction(IsMacroType = true, Category = "Curve Uploader")]
        public static string RegisterCurve([ExcelArgument(AllowReference = false)] object curveName,
                                           [ExcelArgument(AllowReference = false)] object curveSet,
                                           [ExcelArgument(AllowReference = true)] object dateRange,
                                           [ExcelArgument(AllowReference = true)] object valueRange,
                                           [ExcelArgument(AllowReference = true)] object contextID
                                            )
        {
            if (ExcelDnaUtil.IsInFunctionWizard())
                return "";

            return RegisterCurveDetail(curveName, curveSet, dateRange, valueRange, "Other", null, "MONTHLY", contextID);
        }


        [ExcelFunction(IsMacroType = true, Category = "Curve Uploader")]
        public static string RegisterCurveDetail([ExcelArgument(AllowReference = false)] object curveName,
                                           [ExcelArgument(AllowReference = false)] object curveSet,
                                           [ExcelArgument(AllowReference = true)] object dateRange,
                                           [ExcelArgument(AllowReference = true)] object valueRange,
                                           [ExcelArgument(AllowReference = false)] object curveStyle,
                                           [ExcelArgument(AllowReference = false)] object blockName,
                                           [ExcelArgument(AllowReference = false)] object periodicity,
                                           [ExcelArgument(AllowReference = true)] object contextID
                                            )
        {

            if (ExcelDnaUtil.IsInFunctionWizard())
                return "";



            if (curveName == null || curveSet == null || dateRange == null || valueRange == null)
                return null;



            string returnMsg = null;

            try
            {
                Excel.Range dr = AsRange((ExcelReference)dateRange) as Excel.Range;
                Excel.Range vr = AsRange((ExcelReference)valueRange) as Excel.Range;

                // If ranges do not match in length then return error.
                if (dr == null || vr == null || dr.Count != vr.Count)
                    return Global.RegisteredCurveMsgInvalid;

                Excel.Range xlCaller = CallerAsRange() as Excel.Range;

                // Load Curve Marks Business Object (Non-serializable)
                CurveMarksBO curveMarksBO = new CurveMarksBO
                {
                    CallingCell = xlCaller,
                    DateRange = dr,
                    ValueRange = vr,
                    CurveShortName = curveName as string,
                    CurveSet = curveSet as string,
                    CurveStyle = curveStyle as string,
                    BlockName = blockName as string,
                    Periodicity = periodicity as string,
                    ForwardCurveType = ForwardCurveType.PRICE.ToString(),
                    CurveStatus = Global.CurveUploadStatusPending,
                    CurveStatusMsg = "Curve Not Uploaded",
                    CurveStatusDateTime = DateTime.Now,
                    /* Optional Mark Date*/
                    MarkDate = Global.MarkDate ?? DateTime.Now.Date.ToString("o"),
                    ContextID = contextID as string
                };

                if (curveMarksBO == null)
                {
                    _log.Debug("curveMarksBO is null!!!");
                }

                string bookName = "";
                if (((Excel.Application)ExcelDnaUtil.Application).ActiveWorkbook != null)
                    bookName = ((Excel.Application)ExcelDnaUtil.Application).ActiveWorkbook.Name;

                if (_log.IsDebugEnabled && bookName.Equals(""))
                {
                    _log.Debug("bookName is empty!");
                }

                // Dictionary Key definition
                string key = xlCaller.get_Address(xlCaller.Row, xlCaller.Column, Excel.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
                Excel.Worksheet worksheet1 = (Excel.Worksheet)xlCaller.Worksheet;
                key = String.Format("{0}@{1}@{2}", bookName, worksheet1.Index, key.Replace("$", String.Empty));

                // If Curve is already registered then remove it and add the new one.
                // However, preserve the existing curve status
                if (CurveMarksHelper.CurveUploadDictionary.ContainsKey(key))
                {
                    CurveMarksBO temp = CurveMarksHelper.CurveUploadDictionary[key];
                    curveMarksBO.CurveStatus = temp.CurveStatus;
                    curveMarksBO.CurveStatusMsg = temp.CurveStatusMsg;
                    curveMarksBO.CurveStatusDateTime = temp.CurveStatusDateTime;
                    CurveMarksHelper.CurveUploadDictionary.TryRemove(key, out temp);
                }
                _log.Debug("Adding curveMarkBO: " + key + " curveMarksBO: " + curveMarksBO.CurveShortName);
                CurveMarksHelper.CurveUploadDictionary.TryAdd(key, curveMarksBO);


                returnMsg = Global.RegisteredCurveMsgReady;
            }
            catch (Exception e)
            {
                returnMsg = "Error!";
                _log.Error("UDF::RegisterCurveDetail " + e.Message + "\n" + e.StackTrace);
                //System.Windows.Forms.MessageBox.Show(e.Message, "CurveUpload Automation UDF", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            }

            return returnMsg;
        }

        [ExcelFunction(IsMacroType = true, IsVolatile = true, Category = "Curve Uploader")]
        public static string RegisterCurveWithMarkDate([ExcelArgument(AllowReference = false)] object curveName,
                                           [ExcelArgument(AllowReference = false)] object curveSet,
                                           [ExcelArgument(AllowReference = true)] object dateRange,
                                           [ExcelArgument(AllowReference = true)] object valueRange,
                                           [ExcelArgument(AllowReference = false)] object markDate,
                                           [ExcelArgument(AllowReference = true)] object contextID
                                            )
        {
            if (ExcelDnaUtil.IsInFunctionWizard())
                return "";

            return RegisterCurveDetailWithMarkDate(curveName, curveSet, dateRange, valueRange, "Other", null, "MONTHLY", markDate, contextID);
        }


        [ExcelFunction(IsMacroType = true, IsVolatile = true, Category = "Curve Uploader")]
        public static string RegisterCurveDetailWithMarkDate([ExcelArgument(AllowReference = false)] object curveName,
                                           [ExcelArgument(AllowReference = false)] object curveSet,
                                           [ExcelArgument(AllowReference = true)] object dateRange,
                                           [ExcelArgument(AllowReference = true)] object valueRange,
                                           [ExcelArgument(AllowReference = false)] object curveStyle,
                                           [ExcelArgument(AllowReference = false)] object blockName,
                                           [ExcelArgument(AllowReference = false)] object periodicity,
                                           [ExcelArgument(AllowReference = true)] object markDate,
                                           [ExcelArgument(AllowReference = true)] object contextID
                                            )
        {

            if (ExcelDnaUtil.IsInFunctionWizard())
                return "";


            if (!Global.IsUserCurveUploadAdmin)
            {
                return "Uploading Curves with arbitrary Mark Dates is limited to Admins.";
            }


            if (curveName == null || curveSet == null || dateRange == null || valueRange == null)
                return null;

            //if (!DateTime.TryParse(markDate as string, out dtMarkDate))
            //{
            //    return "Invalid Mark Date";
            //}

            DateTime markDateTime = DateTime.Now.Date;
            if (markDate != null && markDate != ExcelDna.Integration.ExcelMissing.Value)
            {
                if (markDate is string)
                {
                    DateTime.TryParse(markDate as string, out markDateTime);

                }
                else if (markDate is ExcelReference)
                {
                    Excel.Range markDateRange = AsRange((ExcelReference)markDate) as Excel.Range;
                    Excel.Range cell = markDateRange.Cells[1, 1];

                    markDateTime = DateTime.FromOADate((double)cell.Value2);
                }
                else
                {
                    markDateTime = DateTime.FromOADate((double)markDate);
                }



            }



            string returnMsg = null;

            try
            {
                Excel.Range dr = AsRange((ExcelReference)dateRange) as Excel.Range;
                Excel.Range vr = AsRange((ExcelReference)valueRange) as Excel.Range;

                // If ranges do not match in length then return error.
                if (dr == null || vr == null || dr.Count != vr.Count)
                    return Global.RegisteredCurveMsgInvalid;

                Excel.Range xlCaller = CallerAsRange() as Excel.Range;

                // Load Curve Marks Business Object (Non-serializable)
                CurveMarksBO curveMarksBO = new CurveMarksBO
                {
                    CallingCell = xlCaller,
                    DateRange = dr,
                    ValueRange = vr,
                    CurveShortName = curveName as string,
                    CurveSet = curveSet as string,
                    CurveStyle = curveStyle as string,
                    BlockName = blockName as string,
                    Periodicity = periodicity as string,
                    ForwardCurveType = ForwardCurveType.PRICE.ToString(),
                    CurveStatus = Global.CurveUploadStatusPending,
                    CurveStatusMsg = "Curve Not Uploaded",
                    CurveStatusDateTime = DateTime.Now,
                    MarkDate = markDateTime.ToString("o"),
                    ContextID = contextID as string
                };

                if (curveMarksBO == null)
                {
                    _log.Debug("curveMarksBO is null!!!");
                }

                string bookName = "";
                if (((Excel.Application)(ExcelDnaUtil.Application)).ActiveWorkbook != null)
                    bookName = ((Excel.Application)(ExcelDnaUtil.Application)).ActiveWorkbook.Name;
                if (_log.IsDebugEnabled && bookName.Equals(""))
                {
                    _log.Debug("bookName is empty!");
                }

                // Dictionary Key definition
                string key = xlCaller.get_Address(xlCaller.Row, xlCaller.Column, Excel.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
                Excel.Worksheet worksheet1 = (Excel.Worksheet)xlCaller.Worksheet;
                key = String.Format("{0}@{1}@{2}", bookName, worksheet1.Index, key.Replace("$", String.Empty));

                // If Curve is already registered then remove it and add the new one.
                // However, preserve the existing curve status
                if (CurveMarksHelper.CurveUploadDictionary.ContainsKey(key))
                {
                    CurveMarksBO temp = CurveMarksHelper.CurveUploadDictionary[key];
                    curveMarksBO.CurveStatus = temp.CurveStatus;
                    curveMarksBO.CurveStatusMsg = temp.CurveStatusMsg;
                    curveMarksBO.CurveStatusDateTime = temp.CurveStatusDateTime;
                    CurveMarksHelper.CurveUploadDictionary.TryRemove(key, out temp);
                }
                if (_log.IsDebugEnabled)
                {
                    _log.Debug("Adding curveMarkBO: " + key + " curveMarksBO: " + curveMarksBO.CurveShortName);
                }
                CurveMarksHelper.CurveUploadDictionary.TryAdd(key, curveMarksBO);


                returnMsg = Global.RegisteredCurveMsgReady;
            }
            catch (Exception e)
            {
                returnMsg = "Error!";
                _log.Error("Uploader::RegisterCurveDetailWithMarkDate " + e.Message + "\n" + e.StackTrace);
                //System.Windows.Forms.MessageBox.Show(e.Message, "CurveUpload Automation UDF", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            }

            return returnMsg;
        }

        [ExcelFunction(IsMacroType = true, Category = "Curve Uploader")]
        public static string RegisterVolatilityCurve([ExcelArgument(AllowReference = false)]object curveName,
                                               [ExcelArgument(AllowReference = false)]object curveSet,
                                               [ExcelArgument(AllowReference = false)]object valueStrike,
                                               [ExcelArgument(AllowReference = true)]object dateRange,
                                               [ExcelArgument(AllowReference = true)]object valueRange)
        {
            if (ExcelDnaUtil.IsInFunctionWizard())
                return "";

            return RegisterVolatilityCurveDetail(curveName, curveSet, valueStrike, dateRange, valueRange,
                                                    "OTHER", null, "MONTHLY");
        }

        [ExcelFunction(IsMacroType = true, Category = "Curve Uploader")]
        public static string RegisterVolatilityCurveDetail([ExcelArgument(AllowReference = false)]object curveName,
                                               [ExcelArgument(AllowReference = false)]object curveSet,
                                               [ExcelArgument(AllowReference = false)]object valueStrike,
                                               [ExcelArgument(AllowReference = true)]object dateRange,
                                               [ExcelArgument(AllowReference = true)]object valueRange,
                                               [ExcelArgument(AllowReference = false)]object curveStyle,
                                               [ExcelArgument(AllowReference = false)]object blockName,
                                               [ExcelArgument(AllowReference = false)]object periodicity)
        {
            if (ExcelDnaUtil.IsInFunctionWizard())
                return string.Empty;





            if (curveName == null || curveSet == null || dateRange == null || valueRange == null)
                return null;

            string returnMsg = null;

            try
            {
                Excel.Range dr = AsRange((ExcelReference)dateRange) as Excel.Range;
                Excel.Range vr = AsRange((ExcelReference)valueRange) as Excel.Range;

                // If ranges do not match in length then return error.
                if (dr == null || vr == null || dr.Count != vr.Count)
                    return Global.RegisteredCurveMsgInvalid;

                // Obtain the calling cell address
                Excel.Range xlCaller = CallerAsRange() as Excel.Range;

                // Load Curve Marks Business Object (Non-serializable)
                CurveMarksBO curveMarksBO = new CurveMarksBO
                {
                    CallingCell = xlCaller,
                    DateRange = dr,
                    ValueRange = vr,
                    CurveShortName = curveName as string,
                    CurveSet = curveSet as string,
                    CurveStyle = curveStyle as string,
                    ValueStrike = Convert.ToString(valueStrike),
                    BlockName = blockName as string,
                    Periodicity = periodicity as string,
                    ForwardCurveType = ForwardCurveType.VOLATILITY.ToString(),
                    CurveStatus = Global.CurveUploadStatusPending,
                    CurveStatusMsg = "Curve Not Uploaded",
                    CurveStatusDateTime = DateTime.Now,
                    /* Optional Mark Date*/
                    MarkDate = Global.MarkDate ?? DateTime.Now.Date.ToString("o")
                };

                string bookName = string.Empty;

                if (((Excel.Application)ExcelDnaUtil.Application).ActiveWorkbook != null)
                    bookName = ((Excel.Application)ExcelDnaUtil.Application).ActiveWorkbook.Name;

                // Dictionary Key definition
                //string key = String.Format("{0}-{1}-{2}", curveName, curveSet, curveMarksBO.CurveStyle);
                string key = xlCaller.get_Address(xlCaller.Row, xlCaller.Column, Excel.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
                Excel.Worksheet worksheet1 = (Excel.Worksheet)xlCaller.Worksheet;
                key = String.Format("{0}@{1}@{2}", bookName, worksheet1.Index, key.Replace("$", String.Empty));

                // If Curve is already registered then remove it and add the new one.
                // However, preserve the existing curve status
                if (CurveMarksHelper.CurveUploadDictionary.ContainsKey(key))
                {
                    CurveMarksBO temp = CurveMarksHelper.CurveUploadDictionary[key];
                    curveMarksBO.CurveStatus = temp.CurveStatus;
                    curveMarksBO.CurveStatusMsg = temp.CurveStatusMsg;
                    curveMarksBO.CurveStatusDateTime = temp.CurveStatusDateTime;
                    CurveMarksHelper.CurveUploadDictionary.TryRemove(key, out temp);
                }
                CurveMarksHelper.CurveUploadDictionary.TryAdd(key, curveMarksBO);

                returnMsg = Global.RegisteredCurveMsgReady;
            }
            catch (Exception e)
            {
                returnMsg = "Error!";
                _log.Error("UDF::RegisterVolatilityCurveDetail", e);
                System.Windows.Forms.MessageBox.Show(e.Message, Global.MessageBoxTitle, System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            }

            return returnMsg;

        }

        [ExcelFunction(IsMacroType = true, IsVolatile = true, Category = "Curve Uploader")]
        public static string RegisterPipelineCurve([ExcelArgument(AllowReference = false)] object curveName,
                                                   [ExcelArgument(AllowReference = false)] object curveSet,
                                                   [ExcelArgument(AllowReference = true)] object dateRange,
                                                    [ExcelArgument(AllowReference = true)] object valueRange
                                                    )
        {
            if (ExcelDnaUtil.IsInFunctionWizard())
                return "";

            return RegisterPipelineCurveDetail(curveName, curveSet, dateRange, valueRange, "OTHER", "DAILY");
        }



        private static string RegisterPipelineCurveDetail([ExcelArgument(AllowReference = false)] object curveName,
                                           [ExcelArgument(AllowReference = false)] object curveSet,
                                           [ExcelArgument(AllowReference = true)] object dateRange,
                                           [ExcelArgument(AllowReference = true)] object valueRange,
                                           [ExcelArgument(AllowReference = false)] object curveStyle,
                                           [ExcelArgument(AllowReference = false)] object periodicity
                                            )
        {

            if (ExcelDnaUtil.IsInFunctionWizard())
                return "";



            if (curveName == null || curveSet == null || dateRange == null || valueRange == null)
                return null;



            string returnMsg = null;

            try
            {
                Excel.Range dr = AsRange((ExcelReference)dateRange) as Excel.Range;
                Excel.Range vr = AsRange((ExcelReference)valueRange) as Excel.Range;


                if (((Excel.Application)ExcelDnaUtil.Application).WorksheetFunction.CountA(dr) <= 0)
                {
                    return Global.RegisteredCurveMsgInvalid;
                }

                // If ranges do not match in length then return error.
                if (dr == null || vr == null || dr.Count != vr.Count)
                    return Global.RegisteredCurveMsgInvalid;

                Excel.Range xlCaller = CallerAsRange() as Excel.Range;


                // Load Curve Marks Business Object (Non-serializable)
                CurveMarksBO curveMarksBO = new CurveMarksBO
                {
                    CallingCell = xlCaller,
                    DateRange = dr,
                    ValueRange = vr,
                    CurveShortName = curveName as string,
                    CurveSet = curveSet as string,
                    CurveStyle = curveStyle as string,
                    BlockName = null,
                    Periodicity = "DAILY",
                    ForwardCurveType = ForwardCurveType.PRICE.ToString(),
                    CurveStatus = Global.CurveUploadStatusPending,
                    CurveStatusMsg = "Curve Not Uploaded",
                    CurveStatusDateTime = DateTime.Now,
                    /* Optional Mark Date*/
                    MarkDate = Global.MarkDate ?? DateTime.Now.Date.ToString("o")
                };


                if (curveMarksBO == null)
                {
                    _log.Debug("curveMarksBO is null!!!");
                }

                curveMarksBO = CurveUploadUtility.LoadPipelineCycleCurveMark(dr, vr, curveMarksBO);

                string bookName = "";
                if (((Excel.Application)ExcelDnaUtil.Application).ActiveWorkbook != null)
                    bookName = ((Excel.Application)ExcelDnaUtil.Application).ActiveWorkbook.Name;
                if (_log.IsDebugEnabled && bookName.Equals(""))
                {
                    _log.Debug("bookName is empty!");
                }

                // Dictionary Key definition
                string key = xlCaller.get_Address(xlCaller.Row, xlCaller.Column, Excel.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
                Excel.Worksheet worksheet1 = (Excel.Worksheet)xlCaller.Worksheet;
                key = String.Format("{0}@{1}@{2}", bookName, worksheet1.Index, key.Replace("$", String.Empty));

                // If Curve is already registered then remove it and add the new one.
                // However, preserve the existing curve status
                if (CurveMarksHelper.CurveUploadDictionary.ContainsKey(key))
                {
                    CurveMarksBO temp = CurveMarksHelper.CurveUploadDictionary[key];
                    curveMarksBO.CurveStatus = temp.CurveStatus;
                    curveMarksBO.CurveStatusMsg = temp.CurveStatusMsg;
                    curveMarksBO.CurveStatusDateTime = temp.CurveStatusDateTime;
                    CurveMarksHelper.CurveUploadDictionary.TryRemove(key, out temp);
                }
                if (_log.IsDebugEnabled)
                {
                    _log.Debug("Adding curveMarkBO: " + key + " curveMarksBO: " + curveMarksBO.CurveShortName);
                }
                CurveMarksHelper.CurveUploadDictionary.TryAdd(key, curveMarksBO);


                returnMsg = Global.RegisteredCurveMsgReady;
            }
            catch (Exception e)
            {
                returnMsg = "Error!" + ":" + e.Message;
                _log.Error("UDF::RegisterCurveDetail " + e.Message + "\n" + e.StackTrace);
                //System.Windows.Forms.MessageBox.Show(e.Message, "CurveUpload Automation UDF", System.Windows.Forms.MessageBoxButtons.OK, System.Windows.Forms.MessageBoxIcon.Error);
            }

            return returnMsg;
        }

        [ExcelFunction(IsMacroType = true, Category = "Curve Uploader")]
        public static object RegisterDownload([ExcelArgument(AllowReference = false)] object curveName,
                                              [ExcelArgument(AllowReference = false)] object curveSet,
                                              [ExcelArgument(AllowReference = true)] object cellPlacement,
                                              [ExcelArgument(AllowReference = true)] object markDate,
                                              [ExcelArgument(AllowReference = true)] object startDate,
                                              [ExcelArgument(AllowReference = true)] object endDate,
                                              [ExcelArgument(AllowReference = true)] object curveStyle,
                                              [ExcelArgument(AllowReference = true)] object blockName,
                                              [ExcelArgument(AllowReference = true)] object contextID
                                              )
        {
            if (ExcelDnaUtil.IsInFunctionWizard())
                return "";

            if (curveName == null || curveName == ExcelDna.Integration.ExcelMissing.Value ||
                curveSet == null || curveSet == ExcelDna.Integration.ExcelMissing.Value ||
                cellPlacement == null || cellPlacement == ExcelDna.Integration.ExcelMissing.Value ||
                markDate == null || markDate == ExcelDna.Integration.ExcelMissing.Value)
                return ExcelError.ExcelErrorNA;

            string returnMsg = string.Empty;
            string sForwardCurveType = string.Empty;
            try
            {

                Excel.Range xlCaller = CallerAsRange() as Excel.Range;

                Excel.Range downloadCell = AsRange((ExcelReference)cellPlacement) as Excel.Range;
                DateTime markDateTime = DateTime.Now.Date;
                if (markDate != null && markDate != ExcelDna.Integration.ExcelMissing.Value)
                {
                    if (markDate is string)
                    {
                        DateTime.TryParse(markDate as string, out markDateTime);
                    }
                    else
                    {
                        Excel.Range markDateRange = AsRange((ExcelReference)markDate) as Excel.Range;
                        Excel.Range cell = markDateRange.Cells[1, 1];
                        markDateTime = DateTime.FromOADate((double)cell.Value2);
                    }
                }
                
                DateTime startDateTime = DateTime.MinValue;
                if (startDate != null && startDate != ExcelDna.Integration.ExcelMissing.Value)
                {
                    if (startDate is string)
                    {
                        DateTime.TryParse(startDate as string, out startDateTime);
                    }
                    else
                    {
                        Excel.Range startDateRange = AsRange((ExcelReference)startDate) as Excel.Range;
                        Excel.Range cell = startDateRange.Cells[1, 1];
                        startDateTime = DateTime.FromOADate((double)cell.Value2);
                    }
                }

                DateTime endDateTime = DateTime.MinValue;

                if (endDate != null && endDate != ExcelDna.Integration.ExcelMissing.Value)
                {
                    if (endDate is string)
                    {
                        DateTime.TryParse(endDate as string, out endDateTime);
                    }
                    else
                    {

                        Excel.Range endDateRange = AsRange((ExcelReference)endDate) as Excel.Range;
                        Excel.Range cell = endDateRange.Cells[1, 1];

                        endDateTime = DateTime.FromOADate((double)cell.Value2);
                    }

                }

                // Load Curve Marks Business Object (Non-serializable)
                CurveMarksBO curveMarksBO = new CurveMarksBO();

                curveMarksBO.CallingCell = xlCaller;
                curveMarksBO.DownloadCell = downloadCell;
                curveMarksBO.CurveShortName = curveName as string;
                curveMarksBO.MarkDate = markDateTime.ToString("d");
                curveMarksBO.CurveSet = curveSet as string;
                curveMarksBO.StartDate = startDateTime == DateTime.MinValue ? null : startDateTime.ToString("d");
                curveMarksBO.EndDate = endDateTime == DateTime.MinValue ? null : endDateTime.ToString("d");
                curveMarksBO.ForwardCurveType = sForwardCurveType;
                curveMarksBO.CurveStyle = string.IsNullOrEmpty(curveStyle as string) ? string.Empty : curveStyle as String;
                curveMarksBO.BlockName = string.IsNullOrEmpty(blockName as string) ? string.Empty : blockName as String;
                curveMarksBO.ContextID = contextID as string;

                string bookName = "";
                Excel.Application application = (Excel.Application)ExcelDnaUtil.Application;

                if (application.ActiveWorkbook != null)
                    bookName = application.ActiveWorkbook.Name;

                // Dictionary Key definition
                string key = xlCaller.get_Address(xlCaller.Row, xlCaller.Column, Excel.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
                string key2 = downloadCell.get_Address(downloadCell.Row, downloadCell.Column, Excel.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
                Excel.Worksheet worksheet1 = (Excel.Worksheet)xlCaller.Worksheet;
                key = String.Format("{0}@{1}@{2}", bookName, worksheet1.Index, key.Replace("$", String.Empty));

                // If Curve is already registered then remove it and add the new one.
                // However, preserve the existing curve status
                if (CurveMarksHelper.CurveDownloadDictionary.ContainsKey(key))
                {
                    CurveMarksBO temp = CurveMarksHelper.CurveDownloadDictionary[key];
                    CurveMarksHelper.CurveDownloadDictionary.TryRemove(key, out temp);
                }
                CurveMarksHelper.CurveDownloadDictionary.TryAdd(key, curveMarksBO);
                
                returnMsg = Global.RegisteredCurveDownloadMsg;
            }
            catch (Exception e)
            {
                returnMsg = "Error!";
                _log.Error("UDF::RegisterDownload " + e.Message + "\n" + e.StackTrace);
            }

            return returnMsg;

        }

        private static object CallerAsRange()
        {
            ExcelReference callerReference = (ExcelReference)XlCall.Excel(XlCall.xlfCaller);
            return AsRange(callerReference);
        }
        
        private static object AsRange(ExcelReference xlref)
        {
            object app = ExcelDnaUtil.Application;
            object refText = XlCall.Excel(XlCall.xlfReftext, xlref, true);
            object range = app.GetType().InvokeMember("Range",
                BindingFlags.Public | BindingFlags.GetProperty,
                null, app, new object[] { refText });
            return range;
        }

        /// <summary>
        /// UploadMultipleMonthlyCurves uploads the curve in the specified range.
        /// </summary>
        /// <param name="dateRange">the dates for which the data is to be uploaded.</param>
        /// <param name="valueRange">the values for which the data is to be uploaded.</param>
        /// <returns></returns>
        [ExcelFunction(IsMacroType = true, Category = "Curve Uploader")]
        public static string UploadMultipleMonthlyCurves([ExcelArgument(AllowReference = true)] object dateRange,
                                           [ExcelArgument(AllowReference = true)] object valueRange)
        {
            if (ExcelDnaUtil.IsInFunctionWizard())
                return String.Empty;

            if (dateRange == null || valueRange == null)
                return null;

            string returnMsg = null;

            try
            {
                Excel.Range dr = AsRange((ExcelReference)dateRange) as Excel.Range;
                Excel.Range vr = AsRange((ExcelReference)valueRange) as Excel.Range;

                if (dr == null || vr == null)
                    return Global.RegisteredCurveMsgInvalid;

                CurveUploadUtility curveUploadUtility = new CurveUploadUtility();
                List<CurveMarksBO> curveMarksBOList = new List<CurveMarksBO>();

                // Calling the GetCurveUploadValuesFromExcelRange to fetch the data within the given range.
                curveMarksBOList = curveUploadUtility.GetCurveUploadValuesFromExcelRange(dr, vr);

                if (curveMarksBOList != null)
                {
                    curveMarksBOList.ForEach(curveMarksBO =>
                           {
                               if (curveMarksBO == null)
                               {
                                   _log.Debug("curveMarksBO is null!!!");
                               }

                               string bookName = "";
                               if (((Excel.Application)ExcelDnaUtil.Application).ActiveWorkbook != null)
                                   bookName = ((Excel.Application)ExcelDnaUtil.Application).ActiveWorkbook.Name;

                               if (_log.IsDebugEnabled && bookName.Equals(""))
                               {
                                   _log.Debug("bookName is empty!");
                               }

                               // Dictionary Key definition
                               string key = curveMarksBO.ValueRange.get_Address(curveMarksBO.ValueRange.Row, curveMarksBO.ValueRange.Column, Excel.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
                               Excel.Worksheet worksheet1 = (Excel.Worksheet)dr.Worksheet;
                               key = String.Format("{0}@{1}@{2}", bookName, worksheet1.Index, key.Replace("$", String.Empty));

                               // If Curve is already registered then remove it and add the new one.
                               // However, preserve the existing curve status
                               if (CurveMarksHelper.CurveUploadDictionary.ContainsKey(key))
                               {
                                   CurveMarksBO temp = CurveMarksHelper.CurveUploadDictionary[key];
                                   curveMarksBO.CurveStatus = temp.CurveStatus;
                                   curveMarksBO.CurveStatusMsg = temp.CurveStatusMsg;
                                   curveMarksBO.CurveStatusDateTime = temp.CurveStatusDateTime;
                                   CurveMarksHelper.CurveUploadDictionary.TryRemove(key, out temp);
                               }
                               _log.Debug("Adding curveMarkBO: " + key + " curveMarksBO: " + curveMarksBO.CurveShortName);
                               CurveMarksHelper.CurveUploadDictionary.TryAdd(key, curveMarksBO);
                               returnMsg = Global.RegisteredCurveMsgReady;
                           });
                    curveUploadUtility.isUploadMultipleMonthlyCurves = true;
                    bool uploadStatus = curveUploadUtility.UploadCurveList(null, false, false);
                    if (uploadStatus)
                    {
                        CurveBuildUtility curveBuildUtility = new CurveBuildUtility();
                        List<CurveBuildStatus> curveBuildStatusList = curveBuildUtility.BuildAllCurves(false);
                    }
                    curveUploadUtility.isUploadMultipleMonthlyCurves = false;
                }
            }
            catch (Exception e)
            {
                returnMsg = "Error!";
                _log.Error("UDF::UploadMultipleMonthlyCurves " + e.Message + "\n" + e.StackTrace);
            }

            return returnMsg;
        }

        /// <summary>
        /// StatusWindow opens the status window to get the curve status.
        /// </summary>
        /// <returns></returns>
        [ExcelFunction(IsMacroType = true, Category = "Curve Uploader")]
        public static object StatusWindow()
        {
            if (ExcelDnaUtil.IsInFunctionWizard())
                return string.Empty;
            try
            {
                FormCurveListStatus.Instance.Show();
            }
            catch (Exception e)
            {
                _log.Error("UDF::StatusWindow " + e.Message + "\n" + e.StackTrace);
            }
            return string.Empty;
        }

        /// <summary>
        /// DownloadMultipleCurves downloads the multiple curves in the specified range.
        /// </summary>
        /// <param name="markDate">the mark date</param>
        /// <param name="parameterRange">the given range of parameters to be passed</param>
        /// <param name="cellPlacement">the output cell</param>
        /// <returns>the downloaded curves with their values</returns>
        [ExcelFunction(IsMacroType = true, Category = "Curve Uploader")]
        public static object DownloadMultipleCurves([ExcelArgument(AllowReference = true)] object markDate,
                                              [ExcelArgument(AllowReference = true)] object parameterRange,
                                              [ExcelArgument(AllowReference = true)] object cellPlacement)
        {
            if (ExcelDnaUtil.IsInFunctionWizard())
                return string.Empty;

            if (markDate == null || markDate == ExcelDna.Integration.ExcelMissing.Value ||
                parameterRange == null || parameterRange == ExcelDna.Integration.ExcelMissing.Value ||
                cellPlacement == null || cellPlacement == ExcelDna.Integration.ExcelMissing.Value)
                return ExcelError.ExcelErrorNA;

            string returnMsg = string.Empty;
            string sForwardCurveType = string.Empty;
            try
            {
                Excel.Range pr = AsRange((ExcelReference)parameterRange) as Excel.Range;
                Excel.Range downloadCell = AsRange((ExcelReference)cellPlacement) as Excel.Range;

                DateTime markDateTime = DateTime.Now.Date;
                if (markDate != null && markDate != ExcelDna.Integration.ExcelMissing.Value)
                {
                    if (markDate is string)
                    {
                        DateTime.TryParse(markDate as string, out markDateTime);
                    }
                    else
                    {
                        Excel.Range markDateRange = AsRange((ExcelReference)markDate) as Excel.Range;
                        Excel.Range cell = markDateRange.Cells[1, 1];
                        markDateTime = DateTime.FromOADate((double)cell.Value2);
                    }
                }

                CurveUploadUtility curveUploadUtility = new CurveUploadUtility();
                List<CurveMarksBO> curveMarksBOList = new List<CurveMarksBO>();
                curveMarksBOList = curveUploadUtility.GetDownloadCurveParameterFromExcelRange(markDateTime, pr, downloadCell);

                string bookName = string.Empty;
                Excel.Application application = (Excel.Application)ExcelDnaUtil.Application;

                if (application.ActiveWorkbook != null)
                    bookName = application.ActiveWorkbook.Name;

                if (curveMarksBOList != null)
                {
                    curveMarksBOList.ForEach(curveMarksBO =>
                           {
                               string key = curveMarksBO.DownloadCell.get_Address(curveMarksBO.DownloadCell.Row, curveMarksBO.DownloadCell.Column, Excel.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
                               Excel.Worksheet worksheet1 = (Excel.Worksheet)pr.Worksheet;
                               key = String.Format("{0}@{1}@{2}", bookName, worksheet1.Index, key.Replace("$", String.Empty));
                               // If Curve is already registered then remove it and add the new one.
                               // However, preserve the existing curve status
                               if (CurveMarksHelper.CurveDownloadDictionary.ContainsKey(key))
                               {
                                   CurveMarksBO temp = CurveMarksHelper.CurveDownloadDictionary[key];
                                   CurveMarksHelper.CurveDownloadDictionary.TryRemove(key, out temp);
                               }
                               CurveMarksHelper.CurveDownloadDictionary.TryAdd(key, curveMarksBO);
                           });
                    curveUploadUtility.isDownloadMultipleMonthlyCurves = true;
                    //  Calling the download curve list.
                    curveUploadUtility.DownloadCurveList(null);
                    curveUploadUtility.isDownloadMultipleMonthlyCurves = false;
                }
                returnMsg = Global.RegisteredCurveDownloadMsg;
            }
            catch (Exception e)
            {
                returnMsg = "Error!";
                _log.Error("UDF::DownloadMultipleCurves " + e.Message + "\n" + e.StackTrace);
            }
            return returnMsg;
        }

        /// <summary>
        /// GetCurveUploaderVersion displays the curve uploader version.
        /// </summary>
        /// <returns>the assembly version number</returns>
        [ExcelFunction(IsMacroType = true, Category = "Curve Uploader")]
        public static string GetCurveUploaderVersion()
        {
            if (ExcelDnaUtil.IsInFunctionWizard())
                return String.Empty;

            string version = null;

            try
            {
                System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
                FileVersionInfo fvi = FileVersionInfo.GetVersionInfo(assembly.Location);
                version = fvi.FileVersion;
            }
            catch (Exception e)
            {
                _log.Error("UDF::GetCurveUploaderVersion " + e.Message + "\n" + e.StackTrace);
            }
            return version;
        }

        /// <summary>
        /// SetServiceHost allows the user to set the service from the excel.
        /// </summary>
        /// <param name="serviceHost"></param>
        /// <returns></returns>
        [ExcelFunction(IsMacroType = true, Category = "Curve Uploader")]
        public static string SetServiceHost([ExcelArgument(AllowReference = true)] string serviceHost)
        {
            if (ExcelDnaUtil.IsInFunctionWizard())
                return String.Empty;

            try
            {
                Global.CurveWebServiceUrl = serviceHost;
            }
            catch (Exception e)
            {
                _log.Error("UDF::SetServiceHost " + e.Message + "\n" + e.StackTrace);
            }
            return Global.CurveWebServiceUrl;
        }

        /// <summary>
        /// Gets the service host for curve uploader.
        /// </summary>
        /// <returns>the string</returns>
        [ExcelFunction(IsMacroType = true, Category = "Curve Uploader")]
        public static string GetServiceHost()
        {
            if (ExcelDnaUtil.IsInFunctionWizard())
                return String.Empty;

            string serviceHost = null;

            try
            {
                serviceHost = Global.CurveWebServiceUrl;
            }
            catch (Exception e)
            {
                _log.Error("UDF::GetServiceHost " + e.Message + "\n" + e.StackTrace);
            }
            return serviceHost;
        }

     

        /// <summary>
        /// DownloadMultipleCurvesByTenor downloads the multiple curves for the specified date range.
        /// </summary>
        /// <param name="markDate">the mark date</param>
        /// <param name="parameterRange">the given range of parameters to be passed</param>
        /// <param name="cellPlacement">the output cell</param>
        /// <param name="dateRange">the date range</param>
        /// <returns>the downloaded curves with their values</returns>
        [ExcelFunction(IsMacroType = true, Category = "Curve Uploader")]
        public static object DownloadMultipleCurvesByTenor([ExcelArgument(AllowReference = true)] object markDate,
                                              [ExcelArgument(AllowReference = true)] object parameterRange,
                                              [ExcelArgument(AllowReference = true)] object dateRange,
                                              [ExcelArgument(AllowReference = true)] object cellPlacement)
        {
            if (ExcelDnaUtil.IsInFunctionWizard())
                return string.Empty;

            if (markDate == null || markDate == ExcelDna.Integration.ExcelMissing.Value ||
                parameterRange == null || parameterRange == ExcelDna.Integration.ExcelMissing.Value ||
                dateRange == null || dateRange == ExcelDna.Integration.ExcelMissing.Value ||
                cellPlacement == null || cellPlacement == ExcelDna.Integration.ExcelMissing.Value)
                return ExcelError.ExcelErrorNA;

            string returnMsg = string.Empty;
            string sForwardCurveType = string.Empty;
            try
            {
                Excel.Range pr = AsRange((ExcelReference)parameterRange) as Excel.Range;
                Excel.Range dr = AsRange((ExcelReference)dateRange) as Excel.Range;
                Excel.Range downloadCell = AsRange((ExcelReference)cellPlacement) as Excel.Range;
                DateTime markDateTime = DateTime.Now.Date;
                if (markDate != null && markDate != ExcelDna.Integration.ExcelMissing.Value)
                {
                    if (markDate is string)
                    {
                        DateTime.TryParse(markDate as string, out markDateTime);
                    }
                    else
                    {
                        Excel.Range markDateRange = AsRange((ExcelReference)markDate) as Excel.Range;
                        Excel.Range cell = markDateRange.Cells[1, 1];
                        markDateTime = DateTime.FromOADate((double)cell.Value2);
                    }
                }

                CurveUploadUtility curveUploadUtility = new CurveUploadUtility();
                List<CurveMarksBO> curveMarksBOList = new List<CurveMarksBO>();
                curveMarksBOList = curveUploadUtility.GetDownloadCurveParameterFromExcelRange(markDateTime, pr, downloadCell);

                string bookName = string.Empty;
                Excel.Application application = (Excel.Application)ExcelDnaUtil.Application;

                if (application.ActiveWorkbook != null)
                    bookName = application.ActiveWorkbook.Name;

                if (curveMarksBOList != null)
                {
                    curveMarksBOList.ForEach(curveMarksBO =>
                    {
                        string key = curveMarksBO.DownloadCell.get_Address(curveMarksBO.DownloadCell.Row, curveMarksBO.DownloadCell.Column, Excel.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
                        Excel.Worksheet worksheet1 = (Excel.Worksheet)pr.Worksheet;
                        key = String.Format("{0}@{1}@{2}", bookName, worksheet1.Index, key.Replace("$", String.Empty));
                        // If Curve is already registered then remove it and add the new one.
                        // However, preserve the existing curve status
                        if (CurveMarksHelper.CurveDownloadDictionary.ContainsKey(key))
                        {
                            CurveMarksBO temp = CurveMarksHelper.CurveDownloadDictionary[key];
                            CurveMarksHelper.CurveDownloadDictionary.TryRemove(key, out temp);
                        }
                        CurveMarksHelper.CurveDownloadDictionary.TryAdd(key, curveMarksBO);
                    });
                    curveUploadUtility.isDownloadMultipleMonthlyCurves = true;
                    //  Calling the download curve list.
                    curveUploadUtility.DownloadCurveList(null, dr);
                    curveUploadUtility.isDownloadMultipleMonthlyCurves = false;
                }
                returnMsg = Global.RegisteredCurveDownloadMsg;
            }
            catch (Exception e)
            {
                returnMsg = "Error!";
                _log.Error("UDF::DownloadMultipleCurvesByTenor " + e.Message + "\n" + e.StackTrace);
            }
            return returnMsg;
        }

        /// <summary>
        /// Gets the current system date.
        /// </summary>
        /// <returns>the string</returns>
        [ExcelFunction(IsMacroType = true, Category = "Curve Uploader")]
        public static string GetCurrentDate()
        {
            if (ExcelDnaUtil.IsInFunctionWizard())
                return String.Empty;

            string serviceHost = null;

            try
            {
                serviceHost = DateTime.Now.ToShortDateString();
            }
            catch (Exception e)
            {
                _log.Error("UDF::GetCurrentDate " + e.Message + "\n" + e.StackTrace);
            }
            return serviceHost;
        }
       
        /// <summary>
        /// CurveUploaderHeartBeat gets the servcie details of the historical service.
        /// </summary>
        /// <returns></returns>
        [ExcelFunction(IsMacroType = true, Category = "Curve Uploader")]
        public static string CurveUploaderHeartBeat()
        {
            string serviceHost = String.Empty;
            if (ExcelDnaUtil.IsInFunctionWizard())
                return String.Empty;

            try
            {
                CurveUploadUtility curveUploadUtility = new CurveUploadUtility();
                serviceHost = curveUploadUtility.CurveUploaderHeartBeat();
            }
            catch (Exception e)
            {
                _log.Error("UDF::CurveUploaderHeartBeat " + e.Message + "\n" + e.StackTrace);
            }
            return serviceHost;
        }     

        /// <summary>
        /// UploadMultipleMonthlyCurvesForVols uploads the curve in the specified range.
        /// </summary>
        /// <param name="dateRange">the dates for which the data is to be uploaded.</param>
        /// <param name="valueRange">the values for which the data is to be uploaded.</param>
        /// <returns></returns>
        [ExcelFunction(IsMacroType = true, Category = "Curve Uploader")]
        public static string UploadMultipleMonthlyCurvesForVols([ExcelArgument(AllowReference = true)] object dateRange,[ExcelArgument(AllowReference = true)] object curveDefRange,
                                           [ExcelArgument(AllowReference = true)] object valueRange, [ExcelArgument(AllowReference = true)] string markDaterange)
        {
            if (ExcelDnaUtil.IsInFunctionWizard())
                return String.Empty;

            if (dateRange == null || curveDefRange == null || valueRange == null)
                return null;

            string returnMsg = null;
            Excel.Range markDtrange = null;

            try
            {
                CurveMarksHelper.CurveUploadDictionary.Clear();
                Excel.Range dtRange = AsRange((ExcelReference)dateRange) as Excel.Range;
                Excel.Range crvDefRange = AsRange((ExcelReference)curveDefRange) as Excel.Range;
                Excel.Range valRange = AsRange((ExcelReference)valueRange) as Excel.Range;                

                if (dtRange == null || crvDefRange == null || valRange == null || markDaterange == null)
                    return Global.RegisteredCurveMsgInvalid;

                CurveUploadUtility curveUploadUtility = new CurveUploadUtility();
                List<CurveMarksBO> curveMarksBOList = new List<CurveMarksBO>();

                // Calling the GetCurveUploadValuesFromExcelRangeForVols to fetch the data within the given range.
                curveMarksBOList = curveUploadUtility.GetCurveUploadValuesFromExcelRangeForVols(dtRange, crvDefRange, valRange, markDaterange);

                if (curveMarksBOList != null)
                {
                    curveMarksBOList.ForEach(curveMarksBO =>
                    {
                        if (curveMarksBO == null)
                        {
                            _log.Debug("curveMarksBO is null!!!");
                        }

                        string bookName = "";
                        if (((Excel.Application)ExcelDnaUtil.Application).ActiveWorkbook != null)
                            bookName = ((Excel.Application)ExcelDnaUtil.Application).ActiveWorkbook.Name;

                        if (_log.IsDebugEnabled && bookName.Equals(""))
                        {
                            _log.Debug("bookName is empty!");
                        }

                        // Dictionary Key definition
                        string key = curveMarksBO.ValueRange.get_Address(curveMarksBO.ValueRange.Row, curveMarksBO.ValueRange.Column, Excel.XlReferenceStyle.xlA1, Type.Missing, Type.Missing);
                        Excel.Worksheet worksheet1 = (Excel.Worksheet)dtRange.Worksheet;
                        key = String.Format("{0}@{1}@{2}", bookName, worksheet1.Index, key.Replace("$", String.Empty));

                        // If Curve is already registered then remove it and add the new one.
                        // However, preserve the existing curve status
                        if (CurveMarksHelper.CurveUploadDictionary.ContainsKey(key))
                        {
                            CurveMarksBO temp = CurveMarksHelper.CurveUploadDictionary[key];
                            curveMarksBO.CurveStatus = temp.CurveStatus;
                            curveMarksBO.CurveStatusMsg = temp.CurveStatusMsg;
                            curveMarksBO.CurveStatusDateTime = temp.CurveStatusDateTime;
                            CurveMarksHelper.CurveUploadDictionary.TryRemove(key, out temp);
                        }
                        _log.Debug("Adding curveMarkBO: " + key + " curveMarksBO: " + curveMarksBO.CurveShortName);
                        CurveMarksHelper.CurveUploadDictionary.TryAdd(key, curveMarksBO);
                        returnMsg = Global.RegisteredCurveMsgReady;
                    });
                    curveUploadUtility.isUploadMultipleMonthlyCurves = true;
                    bool uploadStatus = curveUploadUtility.UploadCurveList(null, false, false);
                    if (uploadStatus)
                    {
                        CurveBuildUtility curveBuildUtility = new CurveBuildUtility();
                        List<CurveBuildStatus> curveBuildStatusList = curveBuildUtility.BuildAllCurves(false);
                    }
                    curveUploadUtility.isUploadMultipleMonthlyCurves = false;
                }
            }
            catch (Exception e)
            {
                returnMsg = "Error!";
                _log.Error("UDF::UploadMultipleMonthlyCurvesForVols " + e.Message + "\n" + e.StackTrace);
            }

            return returnMsg;
        }

    }
}
